<?php
$domain = $_SERVER['HTTP_HOST'];
if ($domain == 'diet.tobe-fit.ru') {
	$ym_id = '44840863';
	$ga_id = 'UA-107926997-3';
	$site_type = 'Диеты (лендинг)';
} else if ($domain == 'sport.tobe-fit.ru') {
	$ym_id = '44002464';
	$ga_id = 'UA-107926997-2';
	$site_type = 'Бодибилдинг (лендинг)';
} else if ($domain == 'pro.tobe-fit.ru') {
	$ym_id = '45314175';
	$ga_id = 'UA-107926997-4';
	$site_type = 'Спортсмены (лендинг)';
} else if ($domain == 'partners.tobe-fit.ru') {
	$ym_id = '';
	$ga_id = 'UA-107926997-5';
	$site_type = 'Партнерская программа (стандарт)';
} else if ($domain == 'coaches.tobe-fit.ru') {
	$ym_id = '';
	$ga_id = 'UA-107926997-6';
	$site_type = 'Партнерская программа (тренеры)';
} else {
	$ym_id = '44840575';
	$ga_id = 'UA-107926997-1';
	$site_type = 'Основной сайт';
}

date_default_timezone_set( 'Europe/Samara' ); // Выставляем временную зону для текущего города

function rus_date() { // Перевод даты на Русский
	// Перевод
	$translate = array(
		"am" => "дп",
		"pm" => "пп",
		"AM" => "ДП",
		"PM" => "ПП",
		"Monday" => "Понедельник",
		"Mon" => "Пн",
		"Tuesday" => "Вторник",
		"Tue" => "Вт",
		"Wednesday" => "Среда",
		"Wed" => "Ср",
		"Thursday" => "Четверг",
		"Thu" => "Чт",
		"Friday" => "Пятница",
		"Fri" => "Пт",
		"Saturday" => "Суббота",
		"Sat" => "Сб",
		"Sunday" => "Воскресенье",
		"Sun" => "Вс",
		"January" => "января",
		"Jan" => "янв",
		"February" => "февраля",
		"Feb" => "фев",
		"March" => "марта",
		"Mar" => "мар",
		"April" => "апреля",
		"Apr" => "апр",
		"May" => "мая",
		"May" => "мая",
		"June" => "июня",
		"Jun" => "июн",
		"July" => "июля",
		"Jul" => "июл",
		"August" => "августа",
		"Aug" => "авг",
		"September" => "сентября",
		"Sep" => "сен",
		"October" => "октября",
		"Oct" => "окт",
		"November" => "ноября",
		"Nov" => "ноя",
		"December" => "декабря",
		"Dec" => "дек",
		"st" => "ое",
		"nd" => "ое",
		"rd" => "е",
		"th" => "ое"
	);
	// если передали дату, то переводим ее
	if (func_num_args() > 1) {
		$timestamp = func_get_arg(1);
		return strtr(date(func_get_arg(0), $timestamp), $translate);
	} else {
		// иначе текущую дату
		return strtr(date(func_get_arg(0)), $translate);
	}
}
$action_date = rus_date('d F', strtotime(date("d.m.Y", time() + 60 * 60 * 24 * 2))); // Читабельная дата +2 дня
?>