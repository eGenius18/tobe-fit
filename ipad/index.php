<?php
if (isset($_POST['contact']))
{
  $valid = array(
      'first_name'  => array('/[A-Za-zА-Яа-яЁё\s]/', 'Введите имя'),
			'last_name'  => array('/[A-Za-zА-Яа-яЁё\s]/', 'Введите фамилию'),
      'phone' => array('/^((8|\+7)[\- ]?)?(\(?\d{3,4}\)?[\- ]?)?[\d\- ]{5,10}$/', 'Введите телефон'),
			'instagram'  => array('/[A-Za-zА-Яа-яЁё\s]/', 'Введите instagram')
  );

  $errors = array();

  foreach ($valid as $field => $data)
  {
    if (isset($_POST[$field]))
    {
      $input = trim($_POST[$field]);

      $regex   = $data[0];
      $message = $data[1];

      if (empty($input) || !preg_match($regex, $input))
      {
        $errors += array($field => $message);
      }
    }
  }

  $post_result = empty($errors) ? 'success' : 'errors';

  if ($post_result === 'success')
  {
    require_once $_SERVER['DOCUMENT_ROOT'].'/ipad/protected/post.php';
  }

  echo json_encode(array
      (
      'post_result' => $post_result,
      'errors' => $errors
  ));
  exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	<title>Купить протеиновые коктейли To be с доставкой по Москве и городам России</title>
	<meta name='description' content='Купить готовые протеиновые коктейли To be c высоким содержанием белка и мицеллярным казеином вы можете в нашем интернет-магазине с доставкой на дом.' />
    <?php include_once($_SERVER['DOCUMENT_ROOT'].'/ipad/templates/head_site.php'); // Стандартные таблицы стилей ?>
</head>
<body>
<div class="container">

    <div class="logo">
        <a href="/">
            <img src="img/logo.png"
                 srcset="img/logo@2x.png 2x,
                         img/logo@3x.png 3x"
                 class="logo"
                 alt="To be">
        </a>
        <div class="logo__text">Протеиновые коктейли<br> для роста мышц от производителя</div>
    </div>

    <div class="topnav">
        <a href="#modal_ipad" class="topnav__order" data-fancybox>ЗАКАЗАТЬ</a>
        <a href="#" class="topnav__contacts">КОНТАКТЫ</a>
    </div>

    <div style="clear: both"></div>

    <h1>Выиграй запас<br> спортпита и сертификат<br> на <span>15 000</span> руб в Nike<br> за 3 простых шага! </h1>

    <div class="main">
        <div class="main__photo">
            <img src="img/photo.png" srcset="img/photo@2x.png 2x, img/photo@3x.png 3x" class="photo">
        </div>
        <div class="steps">
            <div class="steps__cell"><span class="steps__num">01</span> <span class="steps__txt">Зарегистрируйся на стенде То be,<br> нажав кнопку "зарегистироваться"</span></div>
            <div class="steps__cell"><span class="steps__num">02</span> <span class="steps__txt">Подпишись на То be в Инстаграм<br> @tobe_mucsle</span></div>
            <div class="steps__cell"><span class="steps__num">03</span> <span class="steps__txt">29 октября в прямом эфире<br> Инстаграм на нашем стенде<br> мы разыграем приз</span></div>
            <div class="steps__button-nest">
                <img src="img/arrow.png"
                     srcset="img/arrow@2x.png 2x,img/arrow@3x.png 3x"
                     class="arrow">
                <a href="#modal_ipad" class="steps__button" data-fancybox>ЗАРЕГИСТРИРОВАТЬСЯ</a>
            </div>
            <div class="socials">
                <p>Ищите нас в социальных сетях</p>
                <img src="img/fb.png"
                     srcset="img/fb@2x.png 2x,img/fb@3x.png 3x"
                     class="fb">
                <img src="img/vk.png"
                     srcset="img/vk@2x.png 2x,img/vk@3x.png 3x"
                     class="vk">
                <img src="img/insta.png"
                     srcset="img/insta@2x.png 2x,img/insta@3x.png 3x"
                     class="insta">
            </div>
        </div>
    </div>

</div>





<?php include_once($_SERVER['DOCUMENT_ROOT'].'/ipad/templates/modals.php'); // Стандартные всплывайки ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/ipad/templates/foot_site.php'); // Стандартные скрипты ?>


</body>
</html>