<?php
die('mysql_insert');


function escape($s)
{
    if ($s === null || $s === '' || ctype_space($s))
    {
        return '';
    }

    $postlags = defined('ENT_HTML5') ? ENT_QUOTES | ENT_HTML5 : ENT_QUOTES;
    $s     = trim($s);
    $s     = htmlspecialchars($s, $postlags, 'UTF-8');
    return trim($s);
}

$goaltext = array(
    'teaser-form'			=> 'Заявка из тизерного блока',
    'order-form'			=> 'Заказ коктейлей',
    'try-form'				=> 'Заказ пробного набора',
    'contact-form'		=> 'Вопрос со страницы контактов'
);

$post = array();
if (isset($_POST)){
    foreach ($_POST as $key=>$value){
        $post[$key]=escape($value);
    }
}
$post["datetime"] = date('d.m.Y H:i:s');


if(!empty($goaltext[$post['goal']])) {
    $message = array(
        'subject' => "{$goaltext[$post['goal']]}",
        'body'    => "Дата и время: {$post['datetime']}"
        /*. "\n\nДанные клиента"
        . (empty($post['name']) ? '' : "\nИмя: {$post['name']}")
        . (empty($post['phone']) ? '' : "\nТелефон: {$post['phone']}")
        . (empty($post['email']) ? '' : "\nE-mail: {$post['email']}")
        . (empty($post['questb']) ? '' : "\nВопросы: {$post['questb']}")
        . (empty($post['region']) ? '' : "\nОбласть|Край: {$post['region']}")
        . (empty($post['index']) ? '' : "\nИндекс: {$post['index']}")
        . (empty($post['town']) ? '' : "\nНаселенный пункт: {$post['town']}")
        . (empty($post['street']) ? '' : "\nУлица: {$post['street']}")
        . (empty($post['house']) ? '' : "\nДом: {$post['house']}")
        . (empty($post['structure']) ? '' : "\nКорпус|Строение: {$post['structure']}")
        . (empty($post['flat']) ? '' : "\nКвартира: {$post['flat']}")
        . (empty($post['comment']) ? '' : "\nКомментарий: {$post['comment']}")
        . "\n\nЗаказ: "
        . (empty($post['Vanilla']) ? '' : "\nВаниль: {$post['Vanilla']} шт.")
        . (empty($post['Chocolate']) ? '' : "\nШоколад: {$post['Chocolate']} шт.")
        . (empty($post['Strawberry']) ? '' : "\nКлубника: {$post['Strawberry']} шт.")
        . (empty($post['sum']) ? '' : "\nСтоимость: {$post['sum']} руб.")
        . "\n\nЦель: {$goaltext[$post['goal']]}"
        . (empty($post['source']) ? '' : "\n\nСайт: {$post['source']}")
        . (empty($post['campaign']) ? '' : "\nКампания: {$post['campaign']}")*/
    );

    $lang = array(
        'Данные клиента' => array(
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'questb' => 'Вопросы',
            'region' => 'Область/Край',
            'index' => 'Индекс',
            'town' => 'Населенный пункт',
            'street' => 'Улица',
            'house' => 'Дом',
            'structure' => 'Корпус/Строение',
            'flat' => 'Квартира',
            'comment' => 'Комментарий',
        ),
        'Заказ' => array(
            'Vanilla' => 'Ваниль',
            'Chocolate' => 'Шоколад',
            'Strawberry' => 'Клубника',
            'sum' => 'Стоимость',
        ),
        'Дополнительная информация' => array(
            'goal' => 'Цель',
            'source' => 'Сайт',
            'campaign' => 'Кампания',
        )
    );

    foreach($lang as $title => $items) {
        $message['body'] .= PHP_EOL . PHP_EOL . $title . ':';
        foreach($items as $key => $value) {
            if(!empty($post[$key])) {
                $val = $post[$key];
                if('Vanilla' == $key || 'Chocolate' == $key || 'Strawberry' == $key) $val .= ' шт.';
                elseif('goal' == $key) $val = $goaltext[$post['goal']];

                $message['body'] .= PHP_EOL . $value . ': ' . $val;
            }
        }
    }

    $mailHelper = new MailHelper();
    $mailHelper
        ->setTo('liketravelsellrise@mail.ru')
        ->setSubject($message['subject'])
        ->setBody($message['body'])
        ->send();

    $mailHelper2 = new MailHelper();
    $mailHelper2
        ->setTo('tdsportsystems@gmail.com')
        ->setSubject($message['subject'])
        ->setBody($message['body'])
        ->send();

    require_once 'cookies.php';
    $roistatVisitId = array_key_exists('roistat_visit', $_COOKIE) ? $_COOKIE['roistat_visit'] : "неизвестно";
    if( $curl = curl_init() ) {
        curl_setopt($curl, CURLOPT_URL, 'https://aecore.ru/3S1DJpNPbtPbNux_DVFD/form');
        $data = array_merge($_POST, $utm);
        $data['roistat_visit'] = $roistatVisitId;
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl,CURLOPT_HEADER,true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(['form' => $data]));
        $out = curl_exec($curl);
        $error = curl_error($curl);
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
        curl_close($curl);
    }
}