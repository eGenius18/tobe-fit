$(document).ready(function(){
    $('[data-fancybox]').fancybox({
        smallBtn : true,
    });


    $(".components__plus").mouseover(function () {
        var thiz = $(this);
        var color = thiz.hasClass("components__red") ? 'red' : 'yellow';
        thiz.attr('src','/img/minus_'+color+'.png').css({'z-index':3}).next().css({'z-index':2,'display':'block'});
    }).mouseleave(function () {
        var thiz = $(this);
        var color = thiz.hasClass("components__red") ? 'red' : 'yellow';
        setTimeout(function () { //на всякий сделал - вроде подглючивало
            thiz.attr('src','/img/plus_'+color+'.png').css({'z-index':1}).next().css({'z-index':0,'display':'none'});
        },100);
    });

    $(".products__hidden2").click(function (e) {
        e.stopPropagation();
    });

    $(".products__item").mouseover(function () {
        $(this)
            .css({'z-index':3})
            .find(".products__hidden")
            .slideDown(0);
    }).mouseleave(function () {
        $(this)
            .css({'z-index':1})
            .find(".products__hidden")
            .slideUp(0);
    });
    $(".products__choice, .products__img").click(function () {
        // $(".products__item").each(function () { //чистим прошлые клики
        //     $(this).removeClass("products__active");
        // });

        $(this)
			.parents(".products__item")
            .toggleClass("products__active")
			.find(".products__right-type")
            .each(function () {
                $(this).toggleClass("products__hidden2");
            });
        $(this)
            .parents(".products__item")
            .find(".products__details a").toggleClass("black")
    });

    $.fn.toggleButton = function (text, disabledText) {
        var $this = $(this);
        if ($this.attr('disabled')) {
            $this.html(text).removeAttr('disabled');
        }
        else {
            $this.html(disabledText).attr('disabled', 'disabled');
        }
        return this;
    }

    $(document).on("submit", "form", function(e) {
        e.preventDefault();
        var $form = $(this),
            data = $form.serialize();

        var btn_text =  $form.find(":submit").html();

        $form.filter(".post-form").find(":submit").toggleButton(btn_text, "Отправка...");
        $.post($form.attr("action"), data, function(json) {

            if (json.post_result === "success") {
                $form
                    .filter(".post-form")
                    .fadeOut(function() {
                        $(this).siblings(".post-success").fadeIn("slow");
                        $(this).remove();
                    });
            } else if (json.post_result === "exist") {
                $form
                    .filter(".post-form")
                    .fadeOut(function() {
                        $(this).siblings(".post-exist").fadeIn("slow");
                        $(this).remove();
                    });
            }  else if (json.post_result === "failed") {
                $form
                    .filter(".post-form")
                    .fadeOut(function() {
                        $(this).siblings(".post-failed").fadeIn("slow");
                        $(this).remove();
                    });
            }

            setTimeout(function(){location.reload();}, 5000);

            // else {
            //     $form.filter(".post-form").find(":submit").toggleButton(btn_text, "Отправка...");
            //
            //     $('div.errortxt').remove();
            //     $(':input.error').removeClass('error');
            //
            //     $.each(json.errors, function(field, message) {
            //         // Стандартные формы на странице
            //         if ($form.is(".post-form")) {
            //             $form.find("[name=" + field + "]").addClass('error').attr("placeholder", message);
            //         }
            //     });
            // }
        }, "json");
    });




});