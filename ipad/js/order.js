$(document).ready(function(){
	//функционал блока UDS-Game
	/*
    var uds = {
        discount: function() {
            return uds.itemsPrice() * uds.discountBase / 100;
        },
        maxScores: function() {
            var maxScores = Math.floor( ( uds.itemsPrice() - uds.discount() ) * uds.maxScoresDiscount / 100 );
            if(maxScores > uds.scores) maxScores = uds.scores;
            return maxScores;
        },
        itemsPrice: function() {
            var itemsPrice = parseInt($('#order-total').text());
            if(isNaN(itemsPrice)) itemsPrice = 0;
            return itemsPrice;
        },
        discountBase: 0,
        maxScoresDiscount: 0,
        scores: 0,
        customer: '',
        selectors: {},
    };

    $.each([
        'Code',
        'Button',
        'Details',
        'Scores',
        'ScoresButton',
    ], function(i, item) {
        uds.selectors[item] = $('#uds' + item);
    });

    uds.selectors.Scores.change(function() {
        if('' != uds.customer) {
            var scores = parseInt(uds.selectors.Scores.val());
            if (isNaN(scores)) scores = 0;
            maxScores = uds.maxScores();
            if (scores > maxScores) {
                scores = maxScores;
                uds.selectors.Scores.val(0 == scores ? '' : scores);
            }

            var det = [
                '<strong>Промо-код UDS-Game:</strong> ' + uds.selectors.Code.val(),
                '<strong>Пользователь:</strong> ' + uds.customer,
                '<strong>Баллов на счету:</strong> ' + uds.scores,
            ];

            var discount = uds.discount();
            if (0 < discount) det[det.length] = '<strong>Скидка за использование промо-кода:</strong> ' + discount + ' руб';
            if (0 < scores) det[det.length] = '<strong>Использовано баллов:</strong> ' + scores;
            det[det.length] = '<strong>Стоимость товаров с учетом UDS-Game:</strong> ' + ( uds.itemsPrice() - discount - scores ) + ' руб';

            uds.selectors.Details.html('<div>' + det.join('</div><div>') + '</div>');
        } else $(this).val('');
    });

    $('#udsScoresButton, #udsButton').click(function() {
    	if('udsButton' == $(this).prop('id') && uds.selectors.Code.prop('readonly')) uds.selectors.Code.trigger('change');
    	return false;
    });

    uds.selectors.Code.change(function () {
        uds.selectors.Button.html('Проверка кода...');
        if($(this).prop('readonly')) {
            $(this).prop('readonly', false);
            uds.selectors.Button.html('Использовать код');
            $(this).val('');
            uds.selectors.Scores.val('');
            uds.selectors.Details.hide();
            uds.selectors.Scores.hide();
            uds.selectors.ScoresButton.hide();
            uds.customer = '';
        }

        if('' != $(this).val()) {
            $.ajax({
                type: 'post',
                url: setGetParameter(window.location.href, location.hash, 'task', 'ajax'),
                dataType: 'json',
                data: {uds_code: $(this).val()},
                success: function (result) {
                    if (result.error) {
                        alert('Произошла ошибка! Попробуйте пожалуйста позже');
                        console.log(result.content);
                    } else {
                        if(result.content.hasOwnProperty('customer') && !result.content.customer.hasOwnProperty('errorCode')) {
                            uds.selectors.Code.prop('readonly', true);
                            uds.selectors.Button.html('Отменить код');
                            uds.selectors.Details.show();
                            uds.selectors.Scores.show();
                            uds.selectors.ScoresButton.show();

                            uds.discountBase = 'APPLY_DISCOUNT' == result.content.company.baseDiscountPolicy ? result.content.company.marketingSettings.discountBase : 0;
                            uds.maxScoresDiscount = result.content.company.marketingSettings.maxScoresDiscount;
                            uds.scores = result.content.customer.scores;
                            uds.customer = result.content.customer.name + ' ' + result.content.customer.surname;
                            uds.selectors.Scores.trigger('change');
                        } else {
                            uds.selectors.Code.addClass('error');
                        }
                    }
                },
                error: function () {
                    alert('Произошла ошибка! Попробуйте пожалуйста позже');
                }
            });
        }
    });

    if('' != uds.selectors.Code.val()) uds.selectors.Code.trigger('change');
	*/

  // Весь функционал заказа продукта
	$(".products__hidden2").click(function (e) {
		e.stopPropagation();
	});

	$(".products__item").mouseover(function () {
		$(this)
			.css({'z-index':3})
			.find(".products__hidden")
			.slideDown(0);
	}).mouseleave(function () {
		$(this)
			.css({'z-index':1})
			.find(".products__hidden")
			.slideUp(0);
	});
	
	$('.products__choice, .products__img').click( function () {
		if (!$(this).parents('.products__item').hasClass("disabled")) {
			var prod_id = $(this).parents('.products__item').data("prod_id"), 
					prod_price = parseInt($(this).parents('.products__item').data("prod_price")), 
					prod_price_old = parseInt($(this).parents('.products__item').data("prod_price_old")), 
					prod_count = parseInt($('#' + prod_id + '-pcount').html()), 
					prod_text = '1 коробка (24 х 250 г.)',
					total_price = parseInt($('#order-total').text());
					total_price_old = parseInt($('#order-total-old').text());
			if ($(this).parents('.products__item').hasClass("products__active")) {
				total_price = total_price - (prod_price * prod_count);
				total_price_old = total_price_old - (prod_price_old * prod_count);
				$('#' + prod_id + '-pinput').val(0);
				$('#' + prod_id + '-pcount').html(1);
			} else {
				total_price = total_price + (prod_price * prod_count);
				total_price_old = total_price_old + (prod_price_old * prod_count);
				$('#' + prod_id + '-pinput').val(1);
				$('#' + prod_id + '-pcount').html(1);
			}
			$('#preorder-total').text(total_price);
			$('#preorder-total-old').text(total_price_old);
			if (total_price_old > 0) { $('#preorder-total-old').show(); } else { $('#preorder-total-old').hide(); }
			$('#order-total').text(total_price);
			$('#order-total-old').text(total_price_old);
			$('#order-total-inp').val(total_price);
			if (total_price_old > 0) { $('#order-total-old').show(); } else { $('#order-total-old').hide(); }
			//$('#' + prod_id + '-pinfo').fadeToggle("fast", "linear" );
			//$('#' + prod_id + '-pinfo').children('.pinfo-name').click();
			//$('#' + prod_id + '-porder').children('.order-list-text').children('span').html(prod_text);
			//$('#' + prod_id + '-porder').fadeToggle("fast", "linear" );
			$(this)
				.parents(".products__item")
					.toggleClass("products__active")
				.find(".products__right-type")
					.each(function () {
							$(this).toggleClass("products__hidden2");
					});
			$(this)
				.parents(".products__item")
				.find(".products__details a").toggleClass("black")

      //uds.selectors.Scores.trigger('change');
		}
	});
	
	$('.prod-counter').click( function () {
		var prod_id = $(this).parents('.products__item').data("prod_id"), 
				prod_price = parseInt($(this).parents('.products__item').data("prod_price")), 
				prod_price_old = parseInt($(this).parents('.products__item').data("prod_price_old")), 
				prod_count = parseInt($('#' + prod_id + '-pcount').text()), 
				prod_items = 24, 
				prod_text = '', 
				total_price = parseInt($('#order-total').text());
				total_price_old = parseInt($('#order-total-old').text());
		if ($(this).hasClass("minus")) {
			if (prod_count > 1) {
				prod_count = prod_count - 1;
				total_price = total_price - prod_price;
				total_price_old = total_price_old - prod_price_old;
			}
		} else if ($(this).hasClass("plus")) {
			if (prod_count < 9) {
				prod_count = prod_count + 1;
				total_price = total_price + prod_price;
				total_price_old = total_price_old + prod_price_old;
			}
		}
		$('#' + prod_id + '-pinput').val(prod_count);
		$('#' + prod_id + '-pcount').html(prod_count);
		$('#preorder-total').text(total_price);
		$('#preorder-total-old').text(total_price_old);
		if (total_price_old > 0) { $('#preorder-total-old').show(); } else { $('#preorder-total-old').hide(); }
		$('#order-total').text(total_price);
		$('#order-total-old').text(total_price_old);
		$('#order-total-inp').val(total_price);
		if (total_price_old > 0) { $('#order-total-old').show(); } else { $('#order-total-old').hide(); }
		prod_items = prod_count * 24;
		prod_info = prod_items + ' шт.';
		if (prod_count == 1) {
			prod_text = prod_count + ' коробка (' + prod_items + ' х 250 г.)';
		} else if (prod_count < 5) {
			prod_text = prod_count + ' коробки (' + prod_items + ' х 250 г.)';
		} else {
			prod_text = prod_count + ' коробок (' + prod_items + ' х 250 г.)';
		}
		//$('#' + prod_id + '-pinfo').children('.pinfo-name').children('span').html(prod_info);
		//$('#' + prod_id + '-porder').children('.order-list-text').children('span').html(prod_text);

		//uds.selectors.Scores.trigger('change');
	});
	
	/*
	$('.pinfo-name').click( function () {
		$(this).parent().toggleClass("active");
		$(this).siblings('.pinfo-desc').slideToggle("fast", "linear" );
	});
	
	$('.pinfo-desc-point').click( function () {
		$(this).parent().children('.pinfo-desc-point').toggleClass("active");
		$(this).parent().children('.pinfo-dtext').toggle();
	});
	*/
	
	$('.order-list-remove').click( function () {
		var prod_id = $(this).parents('.products__item').data("prod_id"), 
				prod_price = parseInt($(this).parents('.products__item').data("prod_price")), 
				prod_price_old = parseInt($(this).parents('.products__item').data("prod_price_old")), 
				prod_count = parseInt($('#' + prod_id + '-pcount').text()), 
				total_price = parseInt($('#order-total').text());
				total_price_old = parseInt($('#order-total-old').text());
		total_price = total_price - (prod_price * prod_count);
		total_price_old = total_price_old - (prod_price_old * prod_count);
		$('#' + prod_id + '-pinput').val(0);
		$('#' + prod_id + '-pcount').html(1);
		$('#preorder-total').text(total_price);
		$('#preorder-total-old').text(total_price_old);
		if (total_price_old > 0) { $('#preorder-total-old').show(); } else { $('#preorder-total-old').hide(); }
		$('#order-total').text(total_price);
		$('#order-total-old').text(total_price_old);
		$('#order-total-inp').val(total_price);
		if (total_price_old > 0) { $('#order-total-old').show(); } else { $('#order-total-old').hide(); }
		//$('#' + prod_id + '-pinfo').fadeToggle("fast", "linear" );
		//$('#' + prod_id + '-porder').fadeToggle("fast", "linear" );
		$(this)
			.parents(".products__item")
				.toggleClass("products__active")
			.find(".products__right-type")
				.each(function () {
						$(this).toggleClass("products__hidden2");
				});
		$(this)
			.parents(".products__item")
			.find(".products__details a").toggleClass("black")

		//uds.selectors.Scores.trigger('change');
	});
	
	$('div[data-prod_id="1"]').children('.preorder-prod').click();
	
	$('.step-btn').click( function () {
		var step_id = parseInt($(this).data("step"));
		var offsetTop = $("div[id='order-steps']").offset().top;
		$('.order-block').hide();
		$('.order-block[data-step="' + step_id + '"]').fadeIn("slow", "linear" );
		$('body,html').animate({scrollTop:offsetTop}, 900);
		$('.order-step').removeClass("order-step-active");
		$('.order-step[data-step="' + (step_id-1) + '"]').addClass("order-step-passed");
		$('.order-step[data-step="' + step_id + '"]').removeClass("order-step-passed").addClass("order-step-active");
	});
});

function setGetParameter(url, hash, paramName, paramValue)
{
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName));
        var suffix = url.substring(url.indexOf(paramName));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
    }
    else
    {
        if (url.indexOf("?") < 0)
            url += "?" + paramName + "=" + paramValue;
        else
            url += "&" + paramName + "=" + paramValue;
    }
    return url + hash;
}