<?php
include ($_SERVER['DOCUMENT_ROOT'] . '/config/database.php');

function escape($s)
{
  if ($s === null || $s === '' || ctype_space($s))
  {
    return '';
  }

  $flags = defined('ENT_HTML5') ? ENT_QUOTES | ENT_HTML5 : ENT_QUOTES;
  $s     = trim($s);
  $s     = htmlspecialchars($s, $flags, 'UTF-8');
  return trim($s);
}


if ($_POST['goal'] === 'ipad_callback') {

		$first_name = escape($_POST['first_name']);
		$last_name = escape($_POST['last_name']);
		$phone = escape($_POST['phone']);
		$instagram = escape($_POST['instagram']);
		
		
		// Выбираем участников, зареганных в данный момент
		$results = $brise_control->query("SELECT COUNT(*) FROM konkurs WHERE phone = '".$phone."' AND instagram = '".$instagram."' ");
		$total_rows = $results->fetch_row();
		$results->free(); // Удаление выборки

		if ($total_rows[0] > 0) // Если такой участник уже есть
		{
			$post_result = 'exist';
			return;
		}
		else // Если такая игра свободна (или это сертификат)
		{
			$query = "
			INSERT INTO konkurs 
			(first_name, last_name, phone, instagram)	
			VALUES(?, ?, ?, ?)
			";
			$statement = $brise_control->prepare($query);

			// Биндим параметры для маркеров (s = string, i = integer, d = double,  b = blob)
			$statement->bind_param(
				'ssss', 
				$first_name, $last_name, $phone, $instagram
			);

			if(!($statement->execute())){
				$post_result = 'failed';
			}
			
			$statement->close();
			return;
		}

}