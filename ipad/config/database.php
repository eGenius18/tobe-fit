<?php
// Подключение к серверу MySQL
$brise_control = new mysqli(
            'localhost',			/* Хост */ 
            'root',		/* Логин */
            '',	/* Пароль */
            'tobefit');		/* БД */

$brise_control->set_charset("utf8");

// Выводим ошибочку, если у нас проблемсы
if ($brise_control->connect_error) 
{
    die('Error : ('. $brise_control->connect_errno .') '. $brise_control->connect_error);
}
?>