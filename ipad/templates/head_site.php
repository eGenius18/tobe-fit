<link rel="icon" type="image/png" href="/img/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="/img/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/x-icon" href="/img/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="/ipad/css/main.css" />
