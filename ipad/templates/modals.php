<div id="modal_ipad" style="display: none;">
    <form method="post" action="" class="post-form modal-window" autocomplete="off">
        <input type="hidden" name="contact" value="1" />
        <input type="hidden" name="goal" value="ipad_callback" />
            <div class="input-block">
                <label>
                    <div>Ваше имя</div>
                    <input type="text" name="first_name" class="modal-input" placeholder="Ваше имя">
                </label>
                <label>
                    <div>Ваша фамилия</div>
                    <input type="text" name="last_name" class="modal-input" placeholder="Ваша фамилия">
                </label>
                <label>
                    <div>Ваш телефон</div>
                    <input type="text" name="phone" class="modal-input" placeholder="Ваш телефон">
                </label>
                <label>
                    <div>Аккаунт в instagram</div>
                    <input type="text" name="instagram" class="modal-input" placeholder="Аккаунт в instagram">
                </label>
            </div>
        <p>Важно вводить корректные данные, потому что<br> мы будем связываться с победителем<br> по указанному телефону</p>
        <div class="input-button">
            <button type="submit" class="button"><span class="btn-text">ПРИНЯТЬ УЧАСТИЕ</span></button>
        </div>
    </form>
    <div class="post-success form-hidden"><div class="modal-success">ВЫ УСПЕШНО ЗАРЕГИСТРИРОВАЛИСЬ НА<br> УЧАСТИЕ В РОЗЫГРЫШЕ. УДАЧИ!</div></div>
    <div class="post-exist form-hidden"><div class="modal-success">ВЫ УЖЕ ЗАРЕГИСТРИРОВАНЫ В КОНКУРСЕ.<br> ЕСЛИ ЭТО НЕ ТАК - ПОДОЙДИТЕ К МЕНЕДЖЕРУ To be</div></div>
    <div class="post-failed form-hidden"><div class="modal-success">ЧТО-ТО ПОШЛО НЕ ТАК<br> ОБНОВИТЕ СТРАНИЦУ</div></div>
</div>