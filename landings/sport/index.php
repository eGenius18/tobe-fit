<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/main.php');	// Основные фукнции
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/mail_check.php');	// Проверка инпутов на верное заполнение
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	<title>Протеиновые коктейли To be</title>
	<meta name='description' content='' />
	
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/head_site.php'); // Стандартные таблицы стилей ?>
	<link rel="stylesheet" type="text/css" href="/css/landings/sport.css" />
	<link rel="stylesheet" type="text/css" href="/css/order.css" />
	
</head>

<body>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/metrics.php'); // Все метрики ?>

<div class="t1-wrapper wrapper-black">
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/header.php'); // Стандартная шапка ?>
	<div class="teaser-wrapper">
		<div class="container">
			<div class="tsr-inner">
				<div class="tsr-text">Протеиновые коктейли, которые помогают быстро набрать мышечную массу</div>
				<div class="tsr-action">
					<span class="tsr-date">АКЦИЯ! Только до конца октября!</span>
					Стоимость 1 упаковки коктейлей <span class="tsr-small">(24 шт.)</span> всего <span class="tsr-old">3120</span> <span class="tsr-sum">2400<span> руб.</span></span>
				</div>
				<?/*
				<div class="tsr-motivate">Убедитесь в качестве и приятном вкусе коктейлей To be, заказав пробный набор из 3-х вкусов.</div>
				<a href="#main_try" class="tsr-btn btn btn-red-filled">Заказать набор</a>
				<div class="tsr-note">Акция действует в следующих городах: <br/>Москва и МО, Екатеринбург.</div>
				<div class="tsr-motivate">Выберите вкус и закажите коктейли To be с курьерской доставкой до двери.</div>
				*/?>
				<a href="#main_order" class="tsr-btn btn btn-red-filled">Выбрать вкус</a>
			</div>
		</div>
	</div>
</div>

<div class="effects-wrapper wrapper-grey" id="main_effects">
	<div class="container">
		<div class="std-header">С коктейлями To be вы</div>
		<div class="effects-inner">
			<div class="effects-block">
				<img src="/img/land_sport/effect-1.png" alt="" />
				<span>Начнете питаться правильно</span>
				Вам больше не потребуется составлять подходящий рацион. To be - это спортивный питательный комплекс, в котором есть все необходимое для организма в процессе тренировок.
			</div>
			<div class="effects-block">
				<img src="/img/land_sport/effect-2.png" alt="" />
				<span>Повысите эффективность тренировок</span>
				Смесь из быстрых и медленных белков обеспечит ваши мышцы питанием. Благодаря этому вы сможете наращивать мышечную массу быстрее.
			</div>
			<div class="effects-block">
				<img src="/img/land_sport/effect-3.png" alt="" />
				<span>Не будете тратить время</span>
				На смешивание коктейлей в шейкере и приготовление белковых продуктов. Коктейль To be удобно разделён на порции, его можно взять с собой и выпить сразу после тренировки, предотвращая катаболизм
			</div>
			<div class="effects-block">
				<img src="/img/land_sport/effect-4.png" alt="" />
				<span>Станете выглядеть лучше</span>
				Главная составляющая успешных занятий в зале - работа над собой и правильный рацион. Сосредоточьтесь на тренировках, а To be позаботится об остальном.
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="components-wrapper" id="main_components">
	<div class="container">
		<div class="components-images">
			<div class="components-image-l">
				<span class='zoom' id='czoom'>
					<img src="/img/prod-sides/1.jpg" alt="" id="components-iml" />
				</span>
			</div>
			<div class="components-images-s">
				<img src="/img/prod-sides/1.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/2.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/3.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/4.jpg" alt="" class="components-ims" />
			</div>
		</div>
		<div class="components-inner">
			<div class="std-header">Компоненты</div>
			<div class="component-block opened">
				<div class="component-title">Изолят молочного белка</div>
				<div class="component-text">Изолят состоит преимущественно из "быстрых" белков, которые дают необходимую энергию для интенсивных тренировок.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Мицеллярный казеин</div>
				<div class="component-text">Этот белок отличается от изолята медленным усвоением, что позволит восстановить организм после тренировки и избежать катаболизма, разрушающего мышечные волокна.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Обезжиренное молоко</div>
				<div class="component-text">"Основа" коктейлей To be, в котором смешиваются остальные ингредиенты. <br/>Мы используем только качественное молоко одного из крупнейших производителей в России.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Сукралоза</div>
				<div class="component-text">Подсластитель, делающий коктейли по-настоящему вкусными. Это альтернатива сахару, не содержащая калорий и являющаяся безопасной для организма.</div>
			</div>
		</div>
	</div>
</div>

<div class="reviews-wrapper wrapper-grey" id="main_reviews">
	<div class="container">
		<div class="std-header">Что говорят о To be <br/>специалисты и спортсмены</div>
		<div class="reviews-inner">
			<div class="review">
				<div class="review-person">
					<img src="/img/land_sport/reviews/p1.jpg" alt="" />
					<span>Анна Чуракова</span>
					Профессиональная спортсменка, чемпион Европы по версии WBFF
				</div>
				<div class="review-text">
					<span>Это невероятно вкусно и практично</span>
					<p>Рекомендуемая разовая порция протеина, которую удобно взять с собой на тренировку. To be — это вкусный и полезный напиток, уже готовый к употреблению.</p>
					<a data-fancybox="" href="https://www.youtube.com/watch?v=ZFJgdLC-PkE&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0" class="review-btn btn btn-red-bordered">Видеоотзыв</a>
				</div>
				<div class="clearer"></div>
			</div>
		</div>
	</div>
</div>

<?/*
<div class="try-wrapper wrapper-black" id="main_try">
	<div class="container">
		<div class="try-preorder visible">
			<div class="std-header">Закажите пробный набор из 3-х коктейлей To be,</div>
			<div class="std-desc">чтобы подобрать вкус, подходящий именно Вам!</div>
			<div class="try-inner">
				<div class="try-list">
					<div class="try-line">Ваниль <span>250 г.</span></div>
					<div class="try-line">Шоколад <span>250 г.</span></div>
					<div class="try-line">Клубника <span>250 г.</span></div>
				</div>
				<div class="try-note">
					<p>После получения пробного набора <b>вы получите скидку в размере 300 руб.</b> на следующий заказ.</p>
					<p>Акция действует в следующих городах: <br/>Москва и МО, Екатеринбург.</p>
					<p>Количество пробных наборов ограничено!</p>
				</div>
				<div class="try-roll btn btn-red-filled">Заказать</div>
			</div>
		</div>
		<div class="try-order">
			<div class="std-header">Подтвердите заказ <br/>пробного набора</div>
			<div class="try-right">
				<div class="try-list">
					Ваниль, Шоколад, Клубника 
					<span>3 х 250 г.</span>
				</div>
				<div class="try-total">Стоимость набора: <span>300</span></div>
				<div class="try-note">
					<p>После получения пробного набора <b>вы получите скидку в размере 300руб.</b> на следующий заказ.</p>
					<p>Акция действует в следующих городах: <br/>Москва и МО, Екатеринбург.</p>
					<p>Количество пробных наборов ограничено!</p>
				</div>
			</div>
			<div class="try-left">
				<form method="post" action="" class="post-form try-form">
					<input type="hidden" name="contact" value="1" />
					<input type="hidden" name="goal" value="try-form" />
					<div class="input-block-tableft">
						<div class="input-block">
							<label><input type="text" name="name" class="text-input order-input-l" placeholder="Введите ваше имя" /></label>
						</div>
						<div class="input-block">
							<label><input type="text" name="phone" class="text-input order-input-l" placeholder="Введите номер телефона" /></label>
						</div>
						<div class="input-block">
							<label><input type="text" name="email" class="text-input order-input-l" placeholder="Введите ваш email" /></label>
						</div>
					</div>
					<div class="input-block-tabright">
						<div class="input-block input-block-spaced">
							<label><input type="text" name="region" class="text-input order-input-l" placeholder="Область / край" /></label>
						</div>
						<div class="input-block">
							<label><input type="text" name="index" class="text-input order-input-s1" placeholder="Индекс" /></label>
							<label><input type="text" name="town" class="text-input order-input-m2" placeholder="Населенный пункт" /></label>
							<label><input type="text" name="street" class="text-input order-input-m1" placeholder="Улица" /></label>
							<label><input type="text" name="house" class="text-input order-input-s2" placeholder="Дом" /></label>
							<label><input type="text" name="flat" class="text-input order-input-s1" placeholder="Квартира" /></label>
							<label><input type="text" name="structure" class="text-input order-input-m2" placeholder="Корпус / строение" /></label>
						</div>
					</div>
					<div class="input-block-tableft">
						<div class="input-block input-block-spaced">
							<label><textarea rows="2" name="comment" class="text-area order-input-l" placeholder="Комментарий к вашему заказу"></textarea><label>
						</div>
					</div>
					<button type="submit" class="try-btn btn btn-red-filled">Подтвердить</button>
					<div class="try-backroll">Назад</div>
				</form>
				<div class="post-success form-hidden"><div class="try-form-success">Ваша заявка успешно отправлена.<br>Мы перезвоним вам в ближайшее время.</div></div>
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>
*/?>

<div class="prop-wrapper" id="main_properties">
	<div class="container">
		<div class="std-header">5 важных свойств коктейлей To be</div>
		<div class="prop-inner">
			<div class="prop-block">
				<img src="/img/land_sport/prop-1.png" alt="" />
				<span>Приятный <br/>вкус</span>
				To be создан одним из крупнейших в России производителей молока. <br/>
				Наша задача - изготовление по настоящему вкусных продуктов, и To be - не исключение.
			</div>
			<div class="prop-block">
				<img src="/img/land_sport/prop-2.png" alt="" />
				<span>Длительный срок хранения</span>
				Благодаря технологии производства и асептической упаковке срок годности коктейлей составляет 6 месяцев
			</div>
			<div class="prop-block">
				<img src="/img/land_sport/prop-3.png" alt="" />
				<span>Высокое <br/>качество</span>
				Продукт прошел сертификацию и имеет все необходимые лицензии
			</div>
			<div class="clearer"></div>
			<div class="prop-block">
				<img src="/img/land_sport/prop-4.png" alt="" />
				<span>Продуманный состав</span>
				В состав To be входят быстрые и медленные белки. <br/>
				Это позволяет поддерживать организм комплексно - восстанавливая его после тренировки, а также помогая лучше наращивать мышечную массу.
			</div>
			<div class="prop-block">
				<img src="/img/land_sport/prop-5.png" alt="" />
				<span>Низкое <br/>содержание жира</span>
				Коктейль имеет в своем составе всего 0,1-0,2% жира, при этом эффективно утоляя голод на 4-6 часов
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="order-wrapper wrapper-black" id="main_order">
	<div class="container">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/templates/order.php'); // Стандартный блок заказа ?>
	</div>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'); // Подвал сайта ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/modals.php'); // Стандартные всплывайки ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/foot_site.php'); // Стандартные скрипты ?>

</body>
</html>