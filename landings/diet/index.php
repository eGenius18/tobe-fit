<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/main.php');	// Основные фукнции
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/mail_check.php');	// Проверка инпутов на верное заполнение
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	<title>Белковые коктейли для похудения To be</title>
	<meta name='description' content='' />
	
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/head_site.php'); // Стандартные таблицы стилей ?>
	<link rel="stylesheet" type="text/css" href="/css/landings/diet.css" />
	<link rel="stylesheet" type="text/css" href="/css/order.css" />
	
</head>

<body>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/metrics.php'); // Все метрики ?>

<div class="t1-wrapper wrapper-black">
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/header.php'); // Стандартная шапка ?>
	<div class="teaser-wrapper">
		<div class="container">
			<div class="tsr-inner">
				<div class="tsr-text">Белковые коктейли для эффективного похудения</div>
				<ul class="tsr-list">
					<li>Помогают избавиться от лишнего веса</li>
					<li>Снижают аппетит</li>
					<li>Без добавления сахара</li>
				</ul>
				<div class="tsr-action">
					<span class="tsr-date">Летняя цена! Только до конца октября!</span>
					Стоимость 1 упаковки коктейлей <span class="tsr-small">(24 шт.)</span> всего <span class="tsr-old">3120</span> <span class="tsr-sum">2400<span> руб.</span></span>
				</div>
				<?/*
				<div class="tsr-motivate">Убедитесь в качестве и приятном вкусе коктейлей To be, заказав пробный набор из 3-х вкусов.</div>
				<a href="#main_try" class="tsr-btn btn btn-red-filled">Заказать набор</a>
				<div class="tsr-note">Акция действует в следующих городах: <br/>Москва и МО, Екатеринбург.</div>
				<div class="tsr-motivate">Выберите вкус и закажите коктейли To be с курьерской доставкой до двери.</div>
				*/?>
				<a href="#main_order" class="tsr-btn btn btn-red-filled">Выбрать вкус</a>
			</div>
		</div>
	</div>
</div>

<div class="how-wrapper" id="main_how">
	<div class="container">
		<div class="how-inner">
			<div class="std-header">Как белковые коктейли помогают снизить вес?</div>
			<div class="how-text">
				<p class="how-large">Первоочередная задача при похудении - так сформировать рацион, чтобы свести к минимуму объем высокоуглеводной пищи.</p>
				<p>Благодаря сниженному количеству потребляемых углеводов наш организм начинает брать энергию из существующих жировых отложений.</p>
				<p>Однако если составить рацион только из сложных углеводов, организм не будет получать необходимое количество белка и микроэлементов. <br/>В результате дефицита белка становятся тонкими и ломкими ногти, выпадают волосы и начинается ряд других проблем со здоровьем.</p>
				<p>Именно поэтому и возникает необходимость белковых коктейлей, которые содержат все необходимое организму. <br/>Белковые коктейли исключают все возможные последствия для здоровья, при этом позволяя эффективно избавляться от лишнего веса.</p>
			</div>
		</div>
	</div>
</div>

<div class="components-wrapper" id="main_components">
	<div class="container">
		<div class="components-images">
			<div class="components-image-l">
				<span class='zoom' id='czoom'>
					<img src="/img/prod-sides/1.jpg" alt="" id="components-iml" />
				</span>
			</div>
			<div class="components-images-s">
				<img src="/img/prod-sides/1.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/2.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/3.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/4.jpg" alt="" class="components-ims" />
			</div>
		</div>
		<div class="components-inner">
			<div class="std-header">Компоненты</div>
			<div class="component-block opened">
				<div class="component-title">Изолят молочного белка</div>
				<div class="component-text">Содержит в своём составе до 95% натурального белка, который легко усваивается организмом. <br/>Изолят хорошо утоляет чувство голода, не имея в составе жиров и углеводов.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Мицеллярный казеин</div>
				<div class="component-text">Данный белок отличается от изолята медленным усвоением, что позволяет долго не испытывать чувство голода. <br/>При этом длительное отсутствие пищи не вредит организму, так как казеин усваивается постепенно.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Обезжиренное молоко</div>
				<div class="component-text">"Основа" коктейлей To be, в котором смешиваются остальные ингредиенты. <br/>Мы используем только качественное молоко одного из крупнейших производителей в России.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Сукралоза</div>
				<div class="component-text">Подсластитель, делающий коктейли по-настоящему вкусными. Это альтернатива сахару, не содержащая калорий и являющаяся безопасной для организма. </div>
			</div>
		</div>
	</div>
</div>

<div class="properties-wrapper wrapper-black" id="main_properties">
	<div class="container">
		<div class="properties-inner">
			<div class="std-header">Коктейли To be</div>
			<div class="properties-blocks">
				<div class="properties-block">
					<img src="/img/land_diet/prop-1.png" alt="" />
					<span>Абсолютно безопасны</span>
					Все ингредиенты коктейлей To be натуральны и не имеют противопоказаний
				</div>
				<div class="properties-block">
					<img src="/img/land_diet/prop-2.png" alt="" />
					<span>Имеют отличный вкус</span>
					Ванильное мороженое, клубника, шоколад. Все вкусы основаны на одной идее: "Похудение должно быть вкусным!
				</div>
				<div class="properties-block">
					<img src="/img/land_diet/prop-3.png" alt="" />
					<span>Удобно брать с собой</span>
					Коктейль To be удобно разделён на порции, его можно взять с собой и выпить в любое удобное время.
				</div>
				<div class="properties-block">
					<img src="/img/land_diet/prop-4.png" alt="" />
					<span>Долго хранятся</span>
					Благодаря технологии производства и асептической упаковке срок годности коктейлей составляет 6 месяцев
				</div>
			</div>
		</div>
	</div>
</div>

<div class="reviews-wrapper wrapper-grey" id="main_reviews">
	<div class="container">
		<div class="std-header">Прочтите отзывы тех, кто уже достиг результатов с To be</div>
		<div class="reviews-inner">
			<div class="review">
				<div class="review-person">
					<img src="/img/land_diet/reviews/p1.jpg" alt="" />
					<span>Анна Чуракова</span>
					Профессиональная спортсменка, чемпион Европы по версии WBFF
				</div>
				<div class="review-text">
					<span>Белок является компонентом любой сбаланированной диеты</span>
					<p>Белки уменьшают чувство голода и содержат очень мало калорий, при этом являясь важным элементом для организма. Одна упаковка To be содержит оптимальную разовую порцию белка. Это полезный и вкусный напиток, уже готовый к употреблению.</p>
					<a data-fancybox="" href="https://www.youtube.com/watch?v=ZFJgdLC-PkE&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0" class="review-btn btn btn-red-bordered">Видеоотзыв</a>
				</div>
				<div class="clearer"></div>
			</div>
		</div>
	</div>
</div>

<div class="program-wrapper" id="main_program">
	<div class="container">
		<div class="std-header">Получите уникальную программу для похудения,</div>
		<div class="std-desc">основанную на использовании белковых коктейлей To be</div>
		<div class="program-inner">
			<div class="program-block">
				<img src="/img/land_diet/program-1.png" alt="" />
				<span>Диета</span>
				Сбалансированный и вкусный рацион с добавлением коктейлей To be, который ускорит процесс похудения. 
			</div>
			<div class="program-block wide">
				<img src="/img/land_diet/program-2.png" alt="" />
				<span>Программа тренировок</span>
				Список простых упражнений для тренировок в домашних условиях. Это поможет улучшить выносливость и держать мышцы в тонусе
			</div>
			<div class="clearer"></div>
			<a href="#main_order" class="program-btn btn btn-red-filled">Получить программу</a>
		</div>
	</div>
</div>

<div class="order-wrapper wrapper-black" id="main_order">
	<div class="container">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/templates/order.php'); // Стандартный блок заказа ?>
	</div>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'); // Подвал сайта ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/modals.php'); // Стандартные всплывайки ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/foot_site.php'); // Стандартные скрипты ?>
<script type="text/javascript" src="/js/land_diet.js"></script>

</body>
</html>