<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/main.php');	// Основные фукнции
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/mail_check.php');	// Проверка инпутов на верное заполнение
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	<title>Партнерская программа To be для тренеров</title>
	<meta name='description' content='' />
	
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/head_site.php'); // Стандартные таблицы стилей ?>
	<link rel="stylesheet" type="text/css" href="/css/landings/coaches.css" />
	<link rel="stylesheet" type="text/css" href="/css/order.css" />
	
</head>

<body>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/metrics.php'); // Все метрики ?>

<div class="t1-wrapper wrapper-black">
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/header.php'); // Стандартная шапка ?>
	<div class="teaser-wrapper">
		<div class="container">
			<div class="tsr-inner">
				<div class="tsr-text">Зарабатывайте на спортивном питании вместе с To be</div>
				<div class="tsr-text-2">Уникальная партнерская программа для тренеров и менеджеров фитнес-клубов</div>
				<ul class="tsr-list">
					<li>Установите приложение на свой телефон</li>
					<li>Расскажите о To be своим клиентам</li>
					<li>Получайте до 10% от суммы их заказов!</li>
				</ul>
				<div class="tsr-motivate">Чтобы начать зарабатывать - оставьте заявку на участие в программе у нашего менеджера.</div>
			</div>
			<a href="#modal_partners" data-fancybox="" class="tsr-btn btn btn-red-filled">Принять участие</a>
		</div>
	</div>
</div>

<div class="conditions-wrapper wrapper-grey" id="main_conditions">
	<div class="container">
		<div class="std-header">Условия партнерской программы</div>
		<div class="conditions-desc">Чтобы начать зарабатывать, вам нужно рекомендовать To be своим клиентам, создавая клиентскую сеть.</div>
		<div class="conditions-left">
			<div class="conditions-st1">Каждый приглашенный человек приносит Вам 10% от суммы его заказов в виде бонусов</div>
			<div class="conditions-st2">Каждый приглашенный Вашим рефералом (2 уровень), дает Вам 5% от суммы его заказов</div>
			<div class="conditions-st3">От 3 уровня приглашенных Вы получаете 2%</div>
			<div class="conditions-st4">4 уровень приглашенных приносит Вам 1%</div>
		</div>
		<div class="conditions-right">
			<div class="conditions-info" id="conditions-person">Каждый из приглашенных людей при заказе получает 5% на свой бонусный счёт.</div>
			<div class="conditions-info">
				<p>Накопленные баллы можно выводить на банковскую карту, 1 балл равен 1 рублю.</p>
				<p>Начать зарабатывать вместе с To be баллы Вы сможете <a href="#modal_partners" data-fancybox="">после консультации</a> и одобрения заявки нашим менеджером.</p>
			</div>
		</div>
		<div class="clearer"></div>
	</div>
</div>

<div class="sport-wrapper wrapper-black" id="main_sports">
	<div class="container">
		<div class="sport-inner">
			<div class="std-header"><div class="std-header">Коктейли To be могут использоваться для</div></div>
			<div class="sport-blocks">
				<div class="sport-block">
					<img src="/img/land_coaches/sport-1.png" alt="" />
					<span>Силовых видов спорта</span>
					Любые силовые тренировки требуют большого количества белка, который поможет быстрее нарастить мышечную массу и увеличить силу.
				</div>
				<div class="sport-block">
					<img src="/img/land_coaches/sport-2.png" alt="" />
					<span>Кроссфита и единоборств</span>
					Подобные тренировки являются комбинацией из силовых и кардио нагрузок, что требует повышенного внимания к питанию.
				</div>
				<div class="sport-block">
					<img src="/img/land_coaches/sport-3.png" alt="" />
					<span>Других видов спорта</span>
					Коктейли To be помогут вам улучшить результаты в любом спорте, который подразумевает физическую активность.
				</div>
				<div class="sport-block">
					<img src="/img/land_coaches/sport-4.png" alt="" />
					<span>Диет и похудения</span>
					Коктейли To be содержат большое количество белка, при этом являясь низкокалорийными. Это позвоялет использовать их для легкого и безопасного снижения веса.
				</div>
			</div>
		</div>
	</div>
</div>

<div class="steps-wrapper wrapper-grey" id="main_properties">
	<div class="container">
		<div class="std-header">5 простых шагов для участия в партнерской программе</div>
		<div class="steps-inner">
			<div class="steps-block" id="step-1">
				<img src="/img/land_coaches/step-1.png" alt="" />
				<span>Установка приложения</span>
				После одобрения заявки установите приложение на свой телефон и получите персональный промо-код, который понадобится для привлечения людей
			</div>
			<div class="steps-block" id="step-2">
				<img src="/img/land_coaches/step-2.png" alt="" />
				<span>Привлечение людей</span>
				Рассказывайте о наших протеиновых коктейлях своим клиентам (всю необходимую информацию вы можете получить в разделе <a href="#main_materials">материалы</a>)
			</div>
			<div class="steps-block" id="step-3">
				<img src="/img/land_coaches/step-3.png" alt="" />
				<span>Использование промо-кода</span>
				Привлеченные вами люди должны установить приложение, используя <br/>ваш промо-код
			</div>
			<div class="clearer"></div>
			<div class="steps-block" id="step-4">
				<img src="/img/land_coaches/step-4.png" alt="" />
				<span>Получение бонусных баллов</span>
				Каждый привлеченный вами человек при оплате заказа получает 10% от суммы в виде бонусов. Вы также получаете 10% от суммы в виде бонусов
			</div>
			<div class="steps-block" id="step-5">
				<img src="/img/land_coaches/step-5.png" alt="" />
				<span>Вывод бонусных баллов</span>
				Вы можете вывести баллы на свою карту, 1 балл равен 1 рублю
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="materials-wrapper" id="main_materials">
	<div class="container">
		<div class="std-header">Материалы о коктейлях To be,</div>
		<div class="std-desc">которые позволят вам подробно рассказать о преимуществах нашего продукта</div>
		<div class="materials-inner">
			<div class="materials-block">
				<img src="/img/land_coaches/materials-1.png" alt="" />
				<span>Сайты</span>
				Каждый из перечисленных сайтов рассказывает об использовании протеиновых коктейлей To be для конкретных целей:
				<ul>
					<li><a href="http://sport.tobe-fit.ru/" target="_blank">Набор мышечной массы</a></li>
					<li><a href="http://diet.tobe-fit.ru/" target="_blank">Быстрое и эффективное похудение</a></li>
					<li><a href="http://pro.tobe-fit.ru/" target="_blank">Спортивные тренировки и восстановление</a></li>
				</ul>
			</div>
			<div class="materials-block wide">
				<img src="/img/land_coaches/materials-2.png" alt="" />
				<span>Медийные материалы</span>
				Видео и другие полезные материалы, посвященные коктейлям To be. Здесь вы можете найти обзоры известных тренеров и ссылки на соц.сети:
				<ul>
					<li><a href="https://www.youtube.com/watch?v=ZFJgdLC-PkE" target="_blank">Анна Чуракова о коктейлях To be</a></li>
					<li><a href="https://vk.com/tobe_muscle" target="_blank">Группа «To be» Вконтакте</a></li>
					<li><a href="https://www.instagram.com/tobe_muscle/" target="_blank">Instragram коктейлей «To be»</a></li>
				</ul>
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="start-wrapper wrapper-black" id="main_start">
	<div class="container">
		<div class="std-header">Начните зарабатывать на спортивном питании,</div>
		<div class="std-desc">Пользуясь уникальной партнерской программой To be</div>
		<a href="#modal_partners" data-fancybox="" class="tsr-btn btn btn-red-filled">Принять участие</a>
		<div class="start-note">Все подробности партнерской программы вы можете уточнить по телефонам, указанным в разделе контакты, либо у наших менеджеров в соц.сетях</div>
	</div>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'); // Подвал сайта ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/modals.php'); // Стандартные всплывайки ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/foot_site.php'); // Стандартные скрипты ?>

</body>
</html>