<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/main.php');	// Основные фукнции
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/mail_check.php');	// Проверка инпутов на верное заполнение
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	<title>Протеиновые коктейли для тренировок To be</title>
	<meta name='description' content='' />
	
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/head_site.php'); // Стандартные таблицы стилей ?>
	<link rel="stylesheet" type="text/css" href="/css/landings/pro.css" />
	<link rel="stylesheet" type="text/css" href="/css/order.css" />
	
</head>

<body>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/metrics.php'); // Все метрики ?>

<div class="t1-wrapper wrapper-black">
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/header.php'); // Стандартная шапка ?>
	<div class="teaser-wrapper">
		<div class="container">
			<div class="tsr-inner">
				<div class="tsr-text">Протеиновые коктейли, которые помогут вам улучшить результаты в спорте</div>
				<div class="tsr-action">
					<span class="tsr-date">АКЦИЯ! Только до конца октября!</span>
					Стоимость 1 упаковки коктейлей <span class="tsr-small">(24 шт.)</span> всего <span class="tsr-old">3120</span> <span class="tsr-sum">2400<span> руб.</span></span>
				</div>
				<?/*
				<div class="tsr-motivate">Убедитесь в качестве и приятном вкусе коктейлей To be, заказав пробный набор из 3-х вкусов.</div>
				<a href="#main_try" class="tsr-btn btn btn-red-filled">Заказать набор</a>
				<div class="tsr-note">Акция действует в следующих городах: <br/>Москва и МО, Екатеринбург.</div>
				<div class="tsr-motivate">Выберите вкус и закажите коктейли To be с курьерской доставкой до двери.</div>
				*/?>
				<a href="#main_order" class="tsr-btn btn btn-red-filled">Выбрать вкус</a>
			</div>
		</div>
	</div>
</div>

<div class="effects-wrapper wrapper-grey" id="main_effects">
	<div class="container">
		<div class="std-header">Коктейли To be каждый день помогают спортсменам</div>
		<div class="effects-inner">
			<div class="effects-block">
				<img src="/img/land_pro/effect-1.png" alt="" />
				<span>Питаться правильно</span>
				Вам больше не потребуется составлять подходящий рацион. To be - это спортивный питательный комплекс, в котором есть все необходимое для организма в процессе тренировок.
			</div>
			<div class="effects-block">
				<img src="/img/land_pro/effect-2.png" alt="" />
				<span>Повышать эффективность тренировок</span>
				Быстрые белки, входящие в состав коктейлей, позволяют насытить организм энергией, которой хватит для самой интенсивной тренировки.
			</div>
			<div class="effects-block">
				<img src="/img/land_pro/effect-3.png" alt="" />
				<span>Восстанавливать силы</span>
				Медленные белки, которые являются ещё одним компонентом To be, помогут восстановить организм после тренировок и остановить катаболизм.
			</div>
			<div class="effects-block">
				<img src="/img/land_pro/effect-4.png" alt="" />
				<span>Достигать результатов</span>
				Главная составляющая хороших результатов в спорте - постоянные тренировки и правильное питание. Сосредоточьтесь на тренировках, а To be позаботится об остальном.
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="components-wrapper" id="main_components">
	<div class="container">
		<div class="components-images">
			<div class="components-image-l">
				<span class='zoom' id='czoom'>
					<img src="/img/prod-sides/1.jpg" alt="" id="components-iml" />
				</span>
			</div>
			<div class="components-images-s">
				<img src="/img/prod-sides/1.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/2.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/3.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/4.jpg" alt="" class="components-ims" />
			</div>
		</div>
		<div class="components-inner">
			<div class="std-header">Компоненты</div>
			<div class="component-block opened">
				<div class="component-title">Изолят молочного белка</div>
				<div class="component-text">Изолят состоит преимущественно из "быстрых" белков, которые дают необходимую энергию для интенсивных тренировок.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Мицеллярный казеин</div>
				<div class="component-text">Этот белок отличается от изолята медленным усвоением, что позволит восстановить организм после тренировки и избежать катаболизма, разрушающего мышечные волокна.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Обезжиренное молоко</div>
				<div class="component-text">"Основа" коктейлей To be, в котором смешиваются остальные ингредиенты. <br/>Мы используем только качественное молоко одного из крупнейших производителей в России.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Сукралоза</div>
				<div class="component-text">Подсластитель, делающий коктейли по-настоящему вкусными. Это альтернатива сахару, не содержащая калорий и являющаяся безопасной для организма.</div>
			</div>
		</div>
	</div>
</div>

<div class="sport-wrapper wrapper-black" id="main_sports">
	<div class="container">
		<div class="sport-inner">
			<div class="std-header"><div class="std-header">Коктейли To be могут использоваться для</div></div>
			<div class="sport-blocks">
				<div class="sport-block">
					<img src="/img/land_pro/sport-1.png" alt="" />
					<span>Силовых видов спорта</span>
					Любые силовые тренировки требуют большого количества белка, который поможет быстрее нарастить мышечную массу и увеличить силу.
				</div>
				<div class="sport-block">
					<img src="/img/land_pro/sport-2.png" alt="" />
					<span>Велоспорта и легкой атлетики</span>
					Серьёзные кардиотренировки требуют большого количества энергии, а длительные нагрузки требуют правильного восстановления.
				</div>
			</div>
			<div class="sport-block">
				<img src="/img/land_pro/sport-3.png" alt="" />
				<span>Кроссфита и единоборств</span>
				Подобные тренировки являются комбинацией из силовых и кардио нагрузок, что требует повышенного внимания к питанию.
			</div>
			<div class="sport-block">
				<img src="/img/land_pro/sport-4.png" alt="" />
				<span>Других видов спорта</span>
				Коктейли To be помогут вам улучшить результаты в любом спорте, который подразумевает физическую активность.
			</div>
		</div>
	</div>
</div>

<div class="reviews-wrapper wrapper-grey" id="main_reviews">
	<div class="container">
		<div class="std-header">Что говорят о To be <br/>специалисты и спортсмены</div>
		<div class="reviews-inner">
			<div class="review">
				<div class="review-person">
					<img src="/img/land_pro/reviews/p1.jpg" alt="" />
					<span>Анна Чуракова</span>
					Профессиональная спортсменка, чемпион Европы по версии WBFF
				</div>
				<div class="review-text">
					<span>Это невероятно вкусно и практично</span>
					<p>Рекомендуемая разовая порция протеина, которую удобно взять с собой на тренировку. To be — это вкусный и полезный напиток, уже готовый к употреблению.</p>
					<a data-fancybox="" href="https://www.youtube.com/watch?v=ZFJgdLC-PkE&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0" class="review-btn btn btn-red-bordered">Видеоотзыв</a>
				</div>
				<div class="clearer"></div>
			</div>
		</div>
	</div>
</div>

<div class="prop-wrapper" id="main_properties">
	<div class="container">
		<div class="std-header">5 важных свойств коктейлей To be</div>
		<div class="prop-inner">
			<div class="prop-block">
				<img src="/img/land_pro/prop-1.png" alt="" />
				<span>Приятный <br/>вкус</span>
				To be создан одним из крупнейших в России производителей молока. <br/>
				Наша задача - изготовление по настоящему вкусных продуктов, и To be - не исключение.
			</div>
			<div class="prop-block">
				<img src="/img/land_pro/prop-2.png" alt="" />
				<span>Длительный срок хранения</span>
				Благодаря технологии производства и асептической упаковке срок годности коктейлей составляет 6 месяцев
			</div>
			<div class="prop-block">
				<img src="/img/land_pro/prop-3.png" alt="" />
				<span>Высокое <br/>качество</span>
				Продукт прошел сертификацию и имеет все необходимые лицензии
			</div>
			<div class="clearer"></div>
			<div class="prop-block">
				<img src="/img/land_pro/prop-4.png" alt="" />
				<span>Продуманный состав</span>
				В состав To be входят быстрые и медленные белки. <br/>
				Это позволяет поддерживать организм комплексно - обеспечивая его необходимой энергией перед началом тренировки и восстанавливая после.
			</div>
			<div class="prop-block">
				<img src="/img/land_pro/prop-5.png" alt="" />
				<span>Низкое <br/>содержание жира</span>
				Коктейль имеет в своем составе всего 0,1-0,2% жира, при этом эффективно утоляя голод на 4-6 часов
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="order-wrapper wrapper-black" id="main_order">
	<div class="container">
		<?php include($_SERVER['DOCUMENT_ROOT'].'/templates/order.php'); // Стандартный блок заказа ?>
	</div>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'); // Подвал сайта ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/modals.php'); // Стандартные всплывайки ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/foot_site.php'); // Стандартные скрипты ?>

</body>
</html>