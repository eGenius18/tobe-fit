<?php
// Соединяемся с базой (переменная - $brise_control)
include ($_SERVER['DOCUMENT_ROOT'] . '/config/database.php');

$id = $_GET['id'];

// Создаем ассоциативные массивы каждого товара
$results = $brise_control->query("SELECT * FROM cp_products where id='$id'");
while($row = $results->fetch_assoc())
{
    $prod = $row;
//    print_r($row);
//    echo '<br>';
}
$results->free(); // Удаление выборки
?>

<?php
//require_once $_SERVER['DOCUMENT_ROOT'].'/protected/amocrm/index.php';
//include_once($_SERVER['DOCUMENT_ROOT'].'/functions/main.php');	// Основные фукнции
//include_once($_SERVER['DOCUMENT_ROOT'].'/functions/mail_check.php');	// Проверка инпутов на верное заполнение
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />

    <title><?=$prod['pre_name'];?> <?=$prod['name'];?></title>
    <meta name='description' content='Купить готовые протеиновые коктейли To be c высоким содержанием белка и мицеллярным казеином вы можете в нашем интернет-магазине с доставкой на дом.' />

    <?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/head_site.php'); // Стандартные таблицы стилей ?>
    <link rel="stylesheet" type="text/css" href="/css/index.css" />
    <link rel="stylesheet" type="text/css" href="/css/order.css" />

</head>

<body style="background-color: #101010;">

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/metrics.php'); // Все метрики ?>

<div class="modalproddetails">
    <div class="row">
        <div class="col-sm-4 hidden-xs" style="text-align: center"><img src="<?=$prod['image_big'];?>" alt="<?=$prod['name'];?>"></div>
        <div class="col-sm-8 col-xs-12">
            <div class="modalproddetails__header">
                <h1 class="white">
                    <?=$prod['name'];?>
                    <span class="modalproddetails__num">24 шт.</span>
                </h1>
            </div>
            <p class="white">СОСТАВ</p>
            <p><?=$prod['composition'];?></p><br>
<!--            <p class="white">ПИЩЕВАЯ ЦЕННОСТЬ</p>-->
            <p><?=$prod['table_comp'];?></p>
        </div>
    </div>
</div>




<?php //include_once($_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'); // Подвал сайта ?>

<?php //include_once($_SERVER['DOCUMENT_ROOT'].'/templates/modals.php'); // Стандартные всплывайки ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/foot_site.php'); // Стандартные скрипты ?>

</body>
</html>
