<script type="text/javascript" src="/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="/js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/js/jquery.zoom.min.js"></script>
<script type="text/javascript" src="/js/navigation.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/order.js"></script>

<script type="text/javascript">
	function yaReachGoal(goal) {
		try {
			yaCounter<?php echo $ym_id; ?>.reachGoal(goal);
			ga('send', 'event', 'conversion', goal);
		}
		catch (e) {
			console.log("Метрика не установлена. Цель: ", goal);
		}
	}
</script>