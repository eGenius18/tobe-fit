<div class="footer-wrapper wrapper-black">
	<div class="container">
		<div class="footer-inner">
			<div class="footer-col-s">
				<img src="/img/logo-white.png" alt="" />
			</div>
			<div class="footer-col-s">
				<ul>
				<?php if ($domain == 'diet.tobe-fit.ru') { ?>
					<li><a href="#main_components">Компоненты</a></li>
					<li><a href="#main_properties">Свойства</a></li>
					<li><a href="#main_reviews">Отзывы</a></li>
					<li><a href="#main_order">Заказать</a></li>
				<?php } else if ($domain == 'sport.tobe-fit.ru') { ?>
					<li><a href="#main_effects">Преимущества</a></li>
					<li><a href="#main_reviews">Отзывы</a></li>
					<li><a href="#main_properties">Свойства</a></li>
					<li><a href="#main_order">Заказать</a></li>
				<?php } else if ($domain == 'pro.tobe-fit.ru') { ?>
					<li><a href="#main_components">Компоненты</a></li>
					<li><a href="#main_sports">Назначение</a></li>
					<li><a href="#main_properties">Свойства</a></li>
					<li><a href="#main_reviews">Отзывы</a></li>
					<li><a href="#main_order">Заказать</a></li>
				<?php } else { ?>
					<li><a href="/order.php">Заказать</a></li>
					<li><a href="/contacts.php">Контакты</a></li>
				<?php } ?>
				</ul>
			</div>
			<div class="footer-col-m hidden-sm hidden-xs">
				<script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>
				<!-- VK Widget -->
				<div id="vk_groups"></div>
				<script type="text/javascript">
				VK.Widgets.Group("vk_groups", {mode: 3, no_cover: 1}, 142658407);
				</script>
			</div>
			<div class="footer-col-m">
				<div class="footer-info">
					<p>ОАО «МИЛКОМ» <br/>Юридический адрес: 426039, г.Ижевск, Воткинское шоссе, 178, <br/>ИНН 1834100340, КПП 183650001, Расчетный счет 40702810900320102430</p>
					<p>Филиал "Газпромбанк" (АО) <br/>в г.Перми, к/с 30101810200000000808 в ГРКЦ ГУ Банка России <br/>по Пермскому краю БИК 045773808</p>
					<p><a href="/confedential.pdf" target="_blank">Политика конфеденциальности</a></p>
				</div>
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>