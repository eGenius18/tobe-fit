<div id="modal_partners" style="display: none;">
	<form method="post" action="" class="post-form modal-window">
		<div class="modal-header">Заявка на участие в партнерской программе</div>
		<div class="modal-desc">Наш менеджер свяжется с вами и подробно расскажет все подробности участия</div>
		<input type="hidden" name="Contact" value="1" />
		<input type="hidden" name="Goal" value="partners" />
		<div class="modal-inputs">
			<label><input type="text" name="Name" class="modal-input" placeholder="Ваше имя"></label>
			<label><input type="text" name="Phone" class="modal-input" placeholder="Номер телефона"></label>
			<label><input type="text" name="Town" class="modal-input" placeholder="Город"></label>
		</div>
		<div class="input-button">
			<button type="submit" class="modal-btn btn btn-red-filled"><span class="btn-text">Оставить заявку</span></button>
		</div>
		<div class="modal-info">Ваши данные не передаются третьим лицам</div>
	</form>
	<div class="post-success form-hidden"><div class="modal-success">Ваша заявка успешно отправлена.<br>Мы свяжемся с вами в ближайшее время.</div></div>
</div>