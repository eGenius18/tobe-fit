<?php
// Соединяемся с базой (переменная - $brise_control)
include ($_SERVER['DOCUMENT_ROOT'] . '/config/database.php');

// Создаем ассоциативные массивы каждого товара
$results = $brise_control->query("SELECT * FROM cp_products");
while($row = $results->fetch_assoc())
{
	$products_array[$row["id"]] = $row;
}
$results->free(); // Удаление выборки
?>

<div class="order-container">
	<div class="std-header order-header">Закажите коктейли To be с доставкой в Ваш город</div>
	
	<div class="order-steps" id="order-steps">
		<div class="order-step order-step-active" id="order-step-1" data-step="1"><span>Выберите вкусы и количество</span></div>
		<div class="order-step" id="order-step-2" data-step="2"><span>Укажите ваши данные для доставки</span></div>
		<div class="order-step" id="order-step-3" data-step="3"><span>Ожидайте звонка менеджера</span></div>
		<div class="clearer"></div>
	</div>
	
	<div class="order-block" id="order-block-1" data-step="1">
		<div class="xxx-preorder-selector products row">
			<?php foreach($products_array as &$product) { ?>
                <div class="col-xs-6 products__col">
                    <div class="products__item">
                        <img src="<?=$product["image_little"];?>" alt="<?=$product["name"]; ?>" class="products__img">
                        <div class="products__left">
                            <div class="products__pre-name"><?=$product["pre_name"];?></div>
                            <div class="products__name"><?=$product["name"]; ?></div>
                        </div>
                        <div class="products__right">
                            <div class="products__right-type">
                                <div class="products__price">2400 <i class="fa fa-rouble"></i></div>
                                <div class="products__choice">ВЫБРАТЬ</div>
                            </div>
                            <div class="products__right-type products__hidden2 nobr">
                                <div class="products__right-type-wrap"
                                     data-prod_id="<?=$product["id"]; ?>"
                                     data-prod_price="<?=$product["price"];?>"
                                     data-prod_price_old="<?=$product["price_old"];?>"
                                >
                                    <div class="prod-counter preorder-prod-counter minus">-</div>
                                    <div class="prod-counter preorder-prod-count" id="<?php echo $product["id"]; ?>-pcount">1</div>
                                    <div class="prod-counter preorder-prod-counter plus">+</div>
                                </div>
                                <a href="javascript:void(0)" class="order-list-remove">УБРАТЬ</a>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="products__hidden">
                            <div class="products__icons"><img src="/img/icons.png" alt=""></div>
                            <p class="products__details"><a href="/product_modal.php?id=<?=$product["id"];?>" data-fancybox>ПОДРОБНЕЕ</a></p>
                        </div>
                    </div>
                </div>


<?php /*
				<div class="preorder-prod-container <?php echo $product["status"]; ?>" style="border:dashed 1px white" data-prod_id="<?php echo $product["id"]; ?>" data-prod_price="<?php echo $product["price"]; ?>" data-prod_price_old="<?php echo $product["price_old"]; ?>">
					<div class="preorder-prod">
						<div class="img-container">
							<img src="<?php echo $product["image_big"]; ?>" alt="<?php echo $product["name"]; ?>" />
						</div>
						<div class="prod-text"><?php echo $product["name"]; ?></div>
						<div class="prod-tick"></div>
						<div class="prod-status"><?php echo $product["status_text"]; ?></div>
					</div>
					<div class="prod-counter preorder-prod-counters preorder-prod-counter minus">-</div>
					<div class="prod-counter preorder-prod-counters preorder-prod-count" id="<?php echo $product["id"]; ?>-pcount">1</div>
					<div class="prod-counter preorder-prod-counters preorder-prod-counter plus">+</div>
				</div>
*/?>


			<?php } ?>
			<div class="clearer"></div>
		</div>
		<div class="preorder-info">
			<div class="pinfo-title">Ваш заказ:</div>
			<?php foreach($products_array as &$product) { ?>
				<div class="pinfo-text hidden" id="<?php echo $product["id"]; ?>-pinfo">
					<div class="pinfo-name"><?php echo $product["name"]; ?><span>24 шт.</span></div>
					<div class="pinfo-desc hidden">
						<div class="pinfo-desc-point active">Состав</div>
						<div class="pinfo-desc-point">Пищевая ценность</div>
						<div class="pinfo-dtext">
							<?php echo $product["composition"]; ?>
						</div>
						<div class="pinfo-dtext hidden">
							<?php echo $product["table_comp"]; ?>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<div class="clearer"></div>
		<div class="step-btn btn btn-red-filled" data-step="2">ЗАКАЗАТЬ</div>
	</div>

	<div class="order-block hidden" id="order-block-2" data-step="2">
		<div class="order-right">
			<?php foreach($products_array as &$product) { ?>
			<div class="order-list hidden" id="<?php echo $product["id"]; ?>-porder" data-prod_id="<?php echo $product["id"]; ?>" data-prod_price="<?php echo $product["price"]; ?>" data-prod_price_old="<?php echo $product["price_old"]; ?>">
				<img src="<?php echo $product["image_little"]; ?>" alt="<?php echo $product["name"]; ?>" class="order-list-img" />
				<div class="order-list-text">
					<?php echo $product["name"]; ?>
					<span>1 коробка (24 х 250 г.)</span>
				</div>
				<div class="prod-counter order-list-counter minus">-</div>
				<div class="prod-counter order-list-counter plus">+</div>
				<img src="/img/order-basket.png" class="order-list-remove" />
				<div class="clearer"></div>
			</div>
			<?php } ?>
			<div class="order-total">Сумма заказа: <span id="order-total-old">0</span> <span id="order-total">0</span></div>
            <div id="udsDetails"></div>
			<div class="order-note">Доставка от 1 до 3 упаковок коктейлей по Центральной части России осуществляется <span>бесплатно</span> курьером. В остальных случаях стоимость доставки рассчитывается индивидуально при подтверждении</div>
			<div class="order-conf">Предоставляя данную информацию, я согласен с политикой обработки моих персональных данных</div>
		</div>
		<div class="order-left">
			<form method="post" action="" class="post-form order-form">
				<input type="hidden" name="contact" value="1" />
				<input type="hidden" name="goal" value="order-form" />
				<?php foreach($products_array as &$product) { ?>
					<input type="hidden" name="<?php echo $product["name_en"]; ?>" value="0" id="<?php echo $product["id"]; ?>-pinput" />
				<?php } ?>
				<input type="hidden" name="sum" value="0" id="order-total-inp" />
				<div class="input-block-tableft">
					<div class="input-block">
						<label><input type="text" name="name" class="text-input order-input-l" placeholder="Введите ваше имя" /></label>
					</div>
					<div class="input-block">
						<label><input type="text" name="phone" class="text-input order-input-l" placeholder="Введите номер телефона" /></label>
					</div>
					<div class="input-block">
						<label><input type="text" name="email" class="text-input order-input-l" placeholder="Введите ваш email" /></label>
					</div>
                    <div class="uds-block">
                        <div class="input-block input-block-spaced">
                            <label><input type="text" name="udsCode" id="udsCode" class="text-input order-input-l" placeholder="Промо-код UDS-Game" /></label>
                        </div>
                        <button class="btn btn-red-filled" id="udsButton">Использовать код</button>
                        <div class="input-block">
                            <label><input type="text" name="udsScores" id="udsScores" class="text-input order-input-l" placeholder="Количество баллов" /></label>
                        </div>
                        <button class="btn btn-red-filled" id="udsScoresButton">Использовать баллы</button>
                    </div>
				</div>
				<div class="input-block-tabright">
					<div class="input-block input-block-spaced">
						<label><input type="text" name="region" class="text-input order-input-l" placeholder="Область / край" /></label>
					</div>
					<div class="input-block">
						<label><input type="text" name="index" class="text-input order-input-s1" placeholder="Индекс" /></label>
						<label><input type="text" name="town" class="text-input order-input-m2" placeholder="Населенный пункт" /></label>
						<label><input type="text" name="street" class="text-input order-input-m1" placeholder="Улица" /></label>
						<label><input type="text" name="house" class="text-input order-input-s2" placeholder="Дом" /></label>
						<label><input type="text" name="flat" class="text-input order-input-s1" placeholder="Квартира" /></label>
						<label><input type="text" name="structure" class="text-input order-input-m2" placeholder="Корпус / строение" /></label>
					</div>
				</div>
				<div class="input-block-tabright">
					<div class="input-block input-block-spaced">
						<label><textarea rows="2" name="comment" class="text-area order-input-l" placeholder="Комментарий к вашему заказу"></textarea><label>
					</div>
				</div>
				<div class="clearer"></div>
				<button type="submit" class="order-btn btn btn-red-filled" data-step="3">Заказать</button>
				<div class="step-btn step-back" data-step="1">Назад</div>
			</form>
		</div>
		<div class="clearer"></div>
	</div>

	<div class="order-block hidden" id="order-block-3" data-step="3">
		<div class="order-success">
			Благодарим за заказ. <br/> Наш менеджер свяжется с вами в ближайшее время
		</div>
	</div>
	
</div>