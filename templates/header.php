<div class="header-wrapper">
	<div class="container">
		<div class="logo">
			<a href="/"><img src="/img/logo-white.png" alt="" /></a>
			<?php if ($domain == 'diet.tobe-fit.ru') { ?>
				Диетические коктейли с высоким содержанием белка
			<?php } else if ($domain == 'sport.tobe-fit.ru') { ?>
				Протеиновые коктейли для роста мышц от производителя
			<?php } else if ($domain == 'pro.tobe-fit.ru') { ?>
				Протеиновые коктейли для спортсменов от производителя
			<?php } else if ($domain == 'partners.tobe-fit.ru') { ?>
				Бонусная программа протеиновых коктейлей To be
			<?php } else if ($domain == 'coaches.tobe-fit.ru') { ?>
				Партнерская программа To be для тренеров
			<?php } else { ?>
				Протеиновые коктейли для роста мышц от производителя
			<?php } ?>
		</div>
		<div class="header-right">
			<div class="social">
				<a href="https://vk.com/tobe_muscle" target="_blank"><img src="/img/vk.png" alt="" /></a>
				<a href="https://www.instagram.com/tobe_muscle/" target="_blank"><img src="/img/insta.png" alt="" /></a>
				<a href="https://www.facebook.com/tobe.muscle/" target="_blank"><img src="/img/fb.png" alt="" /></a>
			</div>
			<div class="main-navigation" id="site-navigation">
				<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
					<div class="menu-toggle-button"><span class="menu-toggle-inner"></span></div>
				</button>
				<ul class="menu">
				<?php if ($domain == 'diet.tobe-fit.ru') { ?>
					<li><a href="#main_components">Компоненты</a></li>
					<li><a href="#main_properties">Свойства</a></li>
					<li><a href="#main_reviews">Отзывы</a></li>
					<li><a href="#main_order">Заказать</a></li>
				<?php } else if ($domain == 'sport.tobe-fit.ru') { ?>
					<li><a href="#main_effects">Преимущества</a></li>
					<li><a href="#main_reviews">Отзывы</a></li>
					<li><a href="#main_properties">Свойства</a></li>
					<li><a href="#main_order">Заказать</a></li>
				<?php } else if ($domain == 'pro.tobe-fit.ru') { ?>
					<li><a href="#main_components">Компоненты</a></li>
					<li><a href="#main_sports">Назначение</a></li>
					<li><a href="#main_properties">Свойства</a></li>
					<li><a href="#main_reviews">Отзывы</a></li>
					<li><a href="#main_order">Заказать</a></li>
				<?php } else if ($domain == 'partners.tobe-fit.ru') { ?>
					<li><a href="#main_conditions">Условия</a></li>
					<li><a href="#main_sports">О коктейлях</a></li>
					<li><a href="#main_materials">Материалы</a></li>
					<li><a href="#main_start">Участвовать</a></li>
				<?php } else if ($domain == 'coaches.tobe-fit.ru') { ?>
					<li><a href="#main_conditions">Условия</a></li>
					<li><a href="#main_sports">О коктейлях</a></li>
					<li><a href="#main_materials">Материалы</a></li>
					<li><a href="#main_start">Участвовать</a></li>
				<?php } else { ?>
					<li><a href="/order.php">Заказать</a></li>
					<li><a href="http://partners.tobe-fit.ru/" target="_blank">Бонусная программа</a></li>
					<li><a href="http://coaches.tobe-fit.ru/" target="_blank">Тренерам</a></li>
					<li><a href="/contacts.php">Контакты</a></li>
				<?php } ?>
				</ul>
				<div class="clearer"></div>
			</div>
		</div>
	</div>
</div>