<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/main.php');	// Основные фукнции
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/mail_check.php');	// Проверка инпутов на верное заполнение
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	<title>Контакты | To be muscle</title>
	<meta name='description' content='' />
	
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/head_site.php'); // Стандартные таблицы стилей ?>
	<link rel="stylesheet" type="text/css" href="/css/contacts.css" />
	
</head>

<body>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/metrics.php'); // Все метрики ?>

<div class="contacts-wrapper wrapper-black">
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/header.php'); // Стандартная шапка ?>
	<div class="contacts-container">
		<div class="container">
			<div class="contacts-header">Контакты</div>
			<div class="contacts-info">
				<div class="contacts-line">ОАО «МИЛКОМ»</div>
				<div class="contacts-line contacts-line-padded" id="contacts-address">426039, г.Ижевск, Воткинское шоссе, 178</div>
				<div class="contacts-line contacts-line-padded" id="contacts-phone"><a href="tel:+79036232719">+7 (903) 623-27-19</a> (Москва)</div>
				<div class="contacts-line contacts-line-padded" id="contacts-phone"><a href="tel:+79124561960">+7 (912) 456-19-60</a> (Ижевск)</div>
				<div class="contacts-line contacts-line-padded" id="contacts-email"><a href="mailto:info@tobe-fit.ru">info@tobe-fit.ru</a></div>
			</div>
			<div class="contacts-form">
				<div class="contacts-form-header">Связаться с нами</div>
				<form method="post" action="" class="post-form contact-form">
					<input type="hidden" name="contact" value="1">
					<input type="hidden" name="goal" value="contact-form">
					<div class="input-block">
						<label><input type="text" name="name" class="text-input contact-input" placeholder="Введите ваше имя"></label>
					</div>
					<div class="input-block">
						<label><input type="text" name="phone" class="text-input contact-input" placeholder="Введите номер телефона"></label>
					</div>
					<div class="input-block">
						<label><input type="text" name="email" class="text-input contact-input" placeholder="Введите ваш email"></label>
					</div>
					<div class="input-block">
						<label><textarea name="questb" rows="3" class="text-area contact-input" placeholder="Введите вопрос"></textarea></label>
					</div>
					<button type="submit" class="contact-btn btn btn-red-filled">Отправить</button>
					<div class="contact-conf">Предоставляя данную информацию, я согласен с политикой обработки моих персональных данных</div>
				</form>
				<div class="post-success form-hidden"><div class="contact-form-success">Ваш запрос успешно отправлен.<br>Мы свяжемся с вами в ближайшее время.</div></div>
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'); // Подвал сайта ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/modals.php'); // Всплывайки ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/foot_site.php'); // Стандартные скрипты ?>

</body>
</html>