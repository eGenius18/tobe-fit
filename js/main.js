$.fn.toggleButton = function (text, disabledText) {
	var $this = $(this);
	if ($this.attr('disabled')) {
		$this.html(text).removeAttr('disabled');
	}
	else {
		$this.html(disabledText).attr('disabled', 'disabled');
	}
	return this;
}

$(function() {
	$(document)
	.on("submit", "form", function(e) {
		e.preventDefault();
		var $form = $(this),
		data = $form.serialize();

		var btn_text = $form.find(":submit:last").html();
		var step_id = parseInt($form.find(":submit:last").data("step"));

		$form.filter(".post-form").find(":submit:last").toggleButton(btn_text, "Отправка...");

		// var url = $form.attr("action")
		var url = location.href
		$.post(url, data, function(json) {
			if (json.result === "success") {
				yaReachGoal($form.find("[name=goal]").val());
				$form
				.filter(".post-form")
				.fadeOut(function() {
					if ($(this).hasClass("order-form")) {
						$('.order-block').hide();
						$('.order-block[data-step="' + step_id + '"]').fadeIn("slow", "linear" );
						$('.order-step').removeClass("order-step-active");
						$('.order-step[data-step="' + (step_id-1) + '"]').addClass("order-step-passed");
						$('.order-step[data-step="' + step_id + '"]').removeClass("order-step-passed").addClass("order-step-active");
						$(this).remove();
					} else {
						$(this).siblings(".post-success").fadeIn("slow");
						$(this).remove();
					}
				});
			} else {
				$form.filter(".post-form").find(":submit:last").toggleButton(btn_text, "Отправка...");

				$('div.errortxt').remove();
				$(':input.error').removeClass('error');

				$.each(json.errors, function(field, message) {
					// Стандартные формы на странице
					if ($form.is(".post-form")) {
						$form.find("[name=" + field + "]").addClass('error').attr("placeholder", message);
					}
				});
			}
		}, "json");
	});	
});

$(document).ready(function(){ 	
	$( 'a[href^=#main]' ).click( function () {
		var id = $(this).attr( 'href' ).match( /#.+$/ )[0].substr(1);
		var offsetTop = $("div[id='"+ id + "']").offset().top;
		$('body,html').animate({scrollTop:offsetTop}, 900);
		return false;
	});
	
	$("[data-fancybox]").fancybox({
		 smallBtn : true
	});
	
	$(".components__plus").mouseover(function () {
		var thiz = $(this);
		var color = thiz.hasClass("components__red") ? 'red' : 'yellow';
		thiz.attr('src','/img/minus_'+color+'.png').css({'z-index':3}).next().css({'z-index':2,'display':'block'});
	}).mouseleave(function () {
		var thiz = $(this);
		var color = thiz.hasClass("components__red") ? 'red' : 'yellow';
		setTimeout(function () { //на всякий сделал - вроде подглючивало
				thiz.attr('src','/img/plus_'+color+'.png').css({'z-index':1}).next().css({'z-index':0,'display':'none'});
		},100);
	});
	
	$("input[name=phone]").mask("+7(999)999-99-99");
	
	$('.component-block').click( function () {
		$(this).toggleClass("opened");
	});
	
	// Функционал заказа пробников
	$('.try-roll').click( function () {
		$('.try-preorder').fadeToggle("slow", function(){
			$('.try-order').fadeToggle("slow", "linear" );
		});
	});

	$('.try-backroll').click( function () {
		$('.try-order').fadeToggle("slow", function(){
			$('.try-preorder').fadeToggle("slow", "linear" );
		});
	});
	
	if ($(window).width() > '640') { $('#czoom').zoom({magnify: 0.6}); }
	
	$('.components-ims').click( function () {
		img_src = $(this).attr('src');
		$('#components-iml').attr('src',img_src);
		if ($(window).width() > '640') { $('#czoom').zoom({magnify: 0.6}); }
	});
});