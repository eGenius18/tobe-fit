$(document).ready(function(){
    $('[data-fancybox]').fancybox({
        smallBtn : true,
    })


    $(".components__plus").mouseover(function () {
        var thiz = $(this);
        var color = thiz.hasClass("components__red") ? 'red' : 'yellow';
        thiz.attr('src','/img/minus_'+color+'.png').css({'z-index':3}).next().css({'z-index':2,'display':'block'});
    }).mouseleave(function () {
        var thiz = $(this);
        var color = thiz.hasClass("components__red") ? 'red' : 'yellow';
        setTimeout(function () { //на всякий сделал - вроде подглючивало
            thiz.attr('src','/img/plus_'+color+'.png').css({'z-index':1}).next().css({'z-index':0,'display':'none'});
        },100);
    });

    $(".products__hidden2").click(function (e) {
        e.stopPropagation();
    });

    $(".products__item").mouseover(function () {
        $(this)
            .css({'z-index':3})
            .find(".products__hidden")
            .slideDown(0);
    }).mouseleave(function () {
        $(this)
            .css({'z-index':1})
            .find(".products__hidden")
            .slideUp(0);
    });
    $(".products__choice, .products__img").click(function () {
        // $(".products__item").each(function () { //чистим прошлые клики
        //     $(this).removeClass("products__active");
        // });

        $(this)
			.parents(".products__item")
            .toggleClass("products__active")
			.find(".products__right-type")
            .each(function () {
                $(this).toggleClass("products__hidden2");
            });
        $(this)
            .parents(".products__item")
            .find(".products__details a").toggleClass("black")
    });


  // Весь функционал заказа продукта
	$('.products__item').click( function () {
		if (!$(this).hasClass("disabled")) {
			var prod_id = $(this).data("prod_id"), 
					prod_price = parseInt($(this).data("prod_price")), 
					prod_price_old = parseInt($(this).data("prod_price_old")), 
					prod_count = parseInt($('#' + prod_id + '-pcount').html()), 
					prod_text = '1 коробка (24 х 250 г.)',
					total_price = parseInt($('#order-total').text());
					total_price_old = parseInt($('#order-total-old').text());
			if ($(this).hasClass("preordered")) {
				total_price = total_price - (prod_price * prod_count);
				total_price_old = total_price_old - (prod_price_old * prod_count);
				$('#' + prod_id + '-pinput').val(0);
				$('#' + prod_id + '-pcount').html(1);
			} else {
				total_price = total_price + (prod_price * prod_count);
				total_price_old = total_price_old + (prod_price_old * prod_count);
				$('#' + prod_id + '-pinput').val(1);
				$('#' + prod_id + '-pcount').html(1);
			}
			$('#order-total').text(total_price);
			$('#order-total-old').text(total_price_old);
			$('#order-total-inp').val(total_price);
			if (total_price_old > 0) { $('#order-total-old').show(); } else { $('#order-total-old').hide(); }
			$('#' + prod_id + '-pinfo').fadeToggle("fast", "linear" );
			$('#' + prod_id + '-pinfo').children('.pinfo-name').click();
			$('#' + prod_id + '-porder').children('.order-list-text').children('span').html(prod_text);
			$('#' + prod_id + '-porder').fadeToggle("fast", "linear" );
			$(this).toggleClass("preordered");

      //uds.selectors.Scores.trigger('change');
		}
	});
	
	$('.prod-counter').click( function () {
		var prod_id = $(this).parent().data("prod_id"), 
				prod_price = parseInt($(this).parent().data("prod_price")), 
				prod_price_old = parseInt($(this).parent().data("prod_price_old")), 
				prod_count = parseInt($('#' + prod_id + '-pcount').text()), 
				prod_items = 24, 
				prod_text = '', 
				total_price = parseInt($('#order-total').text());
				total_price_old = parseInt($('#order-total-old').text());
		if ($(this).hasClass("minus")) {
			if (prod_count > 1) {
				prod_count = prod_count - 1;
				total_price = total_price - prod_price;
				total_price_old = total_price_old - prod_price_old;
			}
		} else if ($(this).hasClass("plus")) {
			if (prod_count < 9) {
				prod_count = prod_count + 1;
				total_price = total_price + prod_price;
				total_price_old = total_price_old + prod_price_old;
			}
		}
		$('#' + prod_id + '-pinput').val(prod_count);
		$('#' + prod_id + '-pcount').html(prod_count);
		$('#order-total').text(total_price);
		$('#order-total-old').text(total_price_old);
		$('#order-total-inp').val(total_price);
		if (total_price_old > 0) { $('#order-total-old').show(); } else { $('#order-total-old').hide(); }
		prod_items = prod_count * 24;
		prod_info = prod_items + ' шт.';
		if (prod_count == 1) {
			prod_text = prod_count + ' коробка (' + prod_items + ' х 250 г.)';
		} else if (prod_count < 5) {
			prod_text = prod_count + ' коробки (' + prod_items + ' х 250 г.)';
		} else {
			prod_text = prod_count + ' коробок (' + prod_items + ' х 250 г.)';
		}
		$('#' + prod_id + '-pinfo').children('.pinfo-name').children('span').html(prod_info);
		$('#' + prod_id + '-porder').children('.order-list-text').children('span').html(prod_text);

		//uds.selectors.Scores.trigger('change');
	});
	
	$('.pinfo-name').click( function () {
		$(this).parent().toggleClass("active");
		$(this).siblings('.pinfo-desc').slideToggle("fast", "linear" );
	});
	
	$('.pinfo-desc-point').click( function () {
		$(this).parent().children('.pinfo-desc-point').toggleClass("active");
		$(this).parent().children('.pinfo-dtext').toggle();
	});
	
	$('.order-list-remove').click( function () {
		var prod_id = $(this).parent().data("prod_id"), 
				prod_price = parseInt($(this).parent().data("prod_price")), 
				prod_price_old = parseInt($(this).parent().data("prod_price_old")), 
				prod_count = parseInt($('#' + prod_id + '-pcount').text()), 
				total_price = parseInt($('#order-total').text());
				total_price_old = parseInt($('#order-total-old').text());
		total_price = total_price - (prod_price * prod_count);
		total_price_old = total_price_old - (prod_price_old * prod_count);
		$('#' + prod_id + '-pinput').val(0);
		$('#' + prod_id + '-pcount').html(1);
		$('#order-total').text(total_price);
		$('#order-total-old').text(total_price_old);
		$('#order-total-inp').val(total_price);
		if (total_price_old > 0) { $('#order-total-old').show(); } else { $('#order-total-old').hide(); }
		$('#' + prod_id + '-pinfo').fadeToggle("fast", "linear" );
		$('#' + prod_id + '-porder').fadeToggle("fast", "linear" );
		$('.preorder-prod-container[data-prod_id="' + prod_id + '"]').toggleClass("preordered");

		//uds.selectors.Scores.trigger('change');
	});
	
	$('div[data-prod_id="1"]').children('.preorder-prod').click();
	
	$('.step-btn').click( function () {
		var step_id = parseInt($(this).data("step"));
		var offsetTop = $("div[id='order-steps']").offset().top;
		$('.order-block').hide();
		$('.order-block[data-step="' + step_id + '"]').fadeIn("slow", "linear" );
		$('body,html').animate({scrollTop:offsetTop}, 900);
		$('.order-step').removeClass("order-step-active");
		$('.order-step[data-step="' + (step_id-1) + '"]').addClass("order-step-passed");
		$('.order-step[data-step="' + step_id + '"]').removeClass("order-step-passed").addClass("order-step-active");
	});
});

function setGetParameter(url, hash, paramName, paramValue)
{
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName));
        var suffix = url.substring(url.indexOf(paramName));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
    }
    else
    {
        if (url.indexOf("?") < 0)
            url += "?" + paramName + "=" + paramValue;
        else
            url += "&" + paramName + "=" + paramValue;
    }
    return url + hash;
}