<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/protected/amocrm/index.php';
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/main.php');	// Основные фукнции
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/mail_check.php');	// Проверка инпутов на верное заполнение
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	<title>Купить протеиновые коктейли To be с доставкой по Москве и городам России</title>
	<meta name='description' content='Купить готовые протеиновые коктейли To be c высоким содержанием белка и мицеллярным казеином вы можете в нашем интернет-магазине с доставкой на дом.' />
	
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/head_site.php'); // Стандартные таблицы стилей ?>
	<link rel="stylesheet" type="text/css" href="/css/index.css" />
	<link rel="stylesheet" type="text/css" href="/css/order.css" />
	
</head>

<body>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/metrics.php'); // Все метрики ?>


<?php /*
<div class="t1-wrapper wrapper-black">
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/header.php'); // Стандартная шапка ?>
	<div class="teaser-wrapper">
		<div class="container">
			<div class="tsr-inner">
				<h1 class="tsr-text">Протеиновые коктейли для быстрого набора мышечной массы</h1>
				<div class="tsr-action">
					<span class="tsr-date">АКЦИЯ! Только до конца октября!</span>
					Стоимость 1 упаковки коктейлей <span class="tsr-small">(24 шт.)</span> всего <span class="tsr-old">3120</span> <span class="tsr-sum">2400<span> руб.</span></span>
				</div>
				<a href="#main_order" class="tsr-btn btn btn-red-filled">Выбрать вкус</a>
			</div>
		</div>
	</div>
</div>
*/?>

<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/header2.php'); // Стандартная шапка ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 header__title">
                <h1>Коктейли<br> для эффективных тренировок<br> и набора массы</h1>
            </div>
            <div class="col-xs-6 header__left">
                <img src="/img/box_left.jpg" alt="" class="header__box">
                <img src="/img/box_arrow_left.png" alt="" class="header__arrow">
                <a href="#main_order" class="tsr-btn btn btn-red-filled header__button">Выбрать вкус</a>
                <div class="header__desc-left">
                    <h2>TO BE MUSCLE</h2>
                    <p>Протеиновые коктейли для набора мышечной массы</p>
                </div>

            </div>
            <div class="col-xs-6 header__right">
                <img src="/img/box_right.jpg" alt="" class="header__box">
                <img src="/img/box_arrow_right.png" alt="" class="header__arrow">
                <a href="#main_order" class="tsr-btn btn btn-yellow-filled header__button">Выбрать вкус</a>
                <div class="header__desc-right">
                    <h2>TO BE SLIM</h2>
                    <p>Коктейли с L-карнитином для тренировок и сжигания подкожного жира</p>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="effects-wrapper wrapper-grey" id="main_effects">
	<div class="container">
		<div class="std-header">С белковыми коктейлями To be вы</div>
		<div class="effects-inner">
			<div class="effects-block">
				<img src="/img/land_main/effect-1.png" alt="" />
				<span>Начнете питаться правильно</span>
				Вам больше не потребуется составлять подходящий рацион. To be - это спортивный питательный комплекс, в котором есть все необходимое для организма в процессе тренировок.
			</div>
			<div class="effects-block">
				<img src="/img/land_main/effect-2.png" alt="" />
				<span>Повысите эффективность тренировок</span>
				Смесь из быстрых и медленных белков обеспечит ваши мышцы питанием. Благодаря этому вы сможете наращивать мышечную массу быстрее.
			</div>
			<div class="effects-block">
				<img src="/img/land_main/effect-3.png" alt="" />
				<span>Не будете тратить время</span>
				На смешивание коктейлей в шейкере и приготовление белковых продуктов. Коктейль To be удобно разделён на порции, его можно взять с собой и выпить сразу после тренировки, предотвращая катаболизм
			</div>
			<div class="effects-block">
				<img src="/img/land_main/effect-4.png" alt="" />
				<span>Станете выглядеть лучше</span>
				Главная составляющая успешных занятий в зале - работа над собой и правильный рацион. Сосредоточьтесь на тренировках, а To be позаботится об остальном.
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<?php /*
<div class="components-wrapper" id="main_components">
	<div class="container">
		<div class="components-images">
			<div class="components-image-l">
				<span class='zoom' id='czoom'>
					<img src="/img/prod-sides/1.jpg" alt="" id="components-iml" />
				</span>
			</div>
			<div class="components-images-s">
				<img src="/img/prod-sides/1.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/2.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/3.jpg" alt="" class="components-ims" />
				<img src="/img/prod-sides/4.jpg" alt="" class="components-ims" />
			</div>
		</div>
		<div class="components-inner">
			<div class="std-header">Компоненты</div>
			<div class="component-block opened">
				<div class="component-title">Изолят молочного белка</div>
				<div class="component-text">Изолят состоит преимущественно из "быстрых" белков, которые дают необходимую энергию для интенсивных тренировок.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Мицеллярный казеин</div>
				<div class="component-text">Этот белок отличается от изолята медленным усвоением, что позволит восстановить организм после тренировки и избежать катаболизма, разрушающего мышечные волокна.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Обезжиренное молоко</div>
				<div class="component-text">Основа спортивных коктейлей To be, в котором смешиваются остальные ингредиенты. <br/>Мы используем только качественное молоко одного из крупнейших производителей в России.</div>
			</div>
			<div class="component-block">
				<div class="component-title">Сукралоза</div>
				<div class="component-text">Подсластитель, делающий коктейли по-настоящему вкусными. Это альтернатива сахару, не содержащая калорий и являющаяся безопасной для организма.</div>
			</div>
		</div>
	</div>
</div>
*/?>

<div class="container components">
    <div class="row">
        <div class="col-xs-12">
            <h2>Что входит в тренировочный <span class="nobr">комплекс To be?</span></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <h3>Протеиновые коктейли <span class="nobr">To be Muscle</span></h3>
            <p>To be Muscle содержит смесь из быстрых и медленных белков, которые позволяют интенсивно тренироваться и эффективно наращивать мышечную массу, избегая катаболизма после тренировок</p>
        </div>
        <div class="col-xs-6">
            <h3>Коктейли с L-карнитином <span class="nobr">To be Slim</span></h3>
            <p>Этот коктейль содержит L-карнитин - аминокислоту, которая насыщает организм энергией перед тренировкой, повышает выносливость и стимулирует сжигание подкожного жира.</p>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col-xs-12">
            <div class="components__boxes">
                <img src="/img/box_content.jpg" alt="">
                <div>
                    <img src="/img/plus_yellow.png" alt="+" class="components__yellow components__plus components__plus-y1">
                    <div class="components__boxes-desc components__boxes-desc-y1">
                        <h3>L-карнитин</h3>
                        <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                    </div>
                </div>
                <div>
                    <img src="/img/plus_yellow.png" alt="+" class="components__yellow components__plus components__plus-y2">
                    <div class="components__boxes-desc components__boxes-desc-y2">
                        <h3>L-карнитин</h3>
                        <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                    </div>
                </div>
                <div>
                    <img src="/img/plus_yellow.png" alt="+" class="components__yellow components__plus components__plus-y3">
                    <div class="components__boxes-desc components__boxes-desc-y3">
                        <h3>L-карнитин</h3>
                        <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                    </div>
                </div>
                <div>
                    <img src="/img/plus_yellow.png" alt="+" class="components__yellow components__plus components__plus-y4">
                    <div class="components__boxes-desc components__boxes-desc-y4">
                        <h3>L-карнитин</h3>
                        <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                    </div>
                </div>



                <div>
                    <img src="/img/plus_red.png" alt="+" class="components__red components__plus components__plus-r1">
                    <div class="components__boxes-desc components__boxes-desc-r1">
                        <h3>L-карнитин</h3>
                        <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                    </div>
                </div>
                <div>
                    <img src="/img/plus_red.png" alt="+" class="components__red components__plus components__plus-r2">
                    <div class="components__boxes-desc components__boxes-desc-r2">
                        <h3>L-карнитин</h3>
                        <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                    </div>
                </div>
                <div>
                    <img src="/img/plus_red.png" alt="+" class="components__red components__plus components__plus-r3">
                    <div class="components__boxes-desc components__boxes-desc-r3">
                        <h3>L-карнитин</h3>
                        <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                    </div>
                </div>
                <div>
                    <img src="/img/plus_red.png" alt="+" class="components__red components__plus components__plus-r4">
                    <div class="components__boxes-desc components__boxes-desc-r4">
                        <h3>L-карнитин</h3>
                        <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="reviews-wrapper wrapper-grey" id="main_reviews">
	<div class="container">
		<div class="std-header">Отзывы о To be специалистов и спортсменов</div>
		<div class="reviews-inner">
			<div class="review">
				<div class="review-person">
					<img src="/img/land_main/reviews/p1.jpg" alt="" />
					<span>Анна Чуракова</span>
					Профессиональная спортсменка, чемпион Европы по версии WBFF
				</div>
				<div class="review-text">
					<span>Это невероятно вкусно и практично</span>
					<p>Рекомендуемая разовая порция протеина, которую удобно взять с собой на тренировку. To be — это вкусный и полезный напиток, уже готовый к употреблению.</p>
					<a data-fancybox="" href="https://www.youtube.com/watch?v=ZFJgdLC-PkE&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0" class="review-btn btn btn-red-bordered">Видеоотзыв</a>
				</div>
				<div class="clearer"></div>
			</div>
		</div>
	</div>
</div>

<div class="prop-wrapper" id="main_properties">
	<div class="container">
		<div class="std-header">5 важных свойств коктейлей To be</div>
		<div class="prop-inner">
			<div class="prop-block">
				<img src="/img/land_main/prop-1.png" alt="" />
				<span>Приятный <br/>вкус</span>
				To be создан одним из крупнейших в России производителей молока. <br/>
				Наша задача - изготовление по-настоящему вкусных продуктов, и To be - не исключение.
			</div>
			<div class="prop-block">
				<img src="/img/land_main/prop-2.png" alt="" />
				<span>Длительный срок хранения</span>
				Благодаря технологии производства и асептической упаковке, срок годности коктейлей составляет 6 месяцев
			</div>
			<div class="prop-block">
				<img src="/img/land_main/prop-3.png" alt="" />
				<span>Высокое <br/>качество</span>
				Продукт прошел сертификацию и имеет все необходимые лицензии
			</div>
			<div class="clearer"></div>
			<div class="prop-block">
				<img src="/img/land_main/prop-4.png" alt="" />
				<span>Продуманный состав</span>
				В состав To be входят быстрые и медленные белки. <br/>
				Это позволяет поддерживать организм комплексно, восстанавливая его после тренировки, а также помогая лучше наращивать мышечную массу.
			</div>
			<div class="prop-block">
				<img src="/img/land_main/prop-5.png" alt="" />
				<span>Низкое <br/>содержание жира</span>
				Коктейль имеет в своем составе всего 0,1-0,2% жира, при этом эффективно утоляя голод на 4-6 часов
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="order-wrapper wrapper-black" id="main_order">
	<div class="container">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/templates/order2.php'); // Стандартный блок заказа ?>
	</div>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'); // Подвал сайта ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/modals.php'); // Стандартные всплывайки ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/foot_site.php'); // Стандартные скрипты ?>

</body>
</html>