<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/protected/amocrm/index.php';
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/main.php');	// Основные фукнции
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/mail_check.php');	// Проверка инпутов на верное заполнение
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />

	<title>Купить протеиновые коктейли To be с доставкой по Москве и городам России</title>
	<meta name='description' content='Купить готовые протеиновые коктейли To be c высоким содержанием белка и мицеллярным казеином вы можете в нашем интернет-магазине с доставкой на дом.' />
	
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/head_site.php'); // Стандартные таблицы стилей ?>
	<link rel="stylesheet" type="text/css" href="/css/index.css" />
	<link rel="stylesheet" type="text/css" href="/css/order.css" />
	
</head>

<body>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/metrics.php'); // Все метрики ?>

<?php if (strstr($_SERVER['DOCUMENT_ROOT'],'http_server') || @$_GET['test']=='test') :?>
    <div class="container" style="background-color:green; z-index:100; position: fixed; height: 25px;">
        <div class="row">
            <div class="col boot-xs visible-xs">xs</div>
            <div class="col boot-sm visible-sm">sm</div>
            <div class="col boot-md visible-md">md</div>
            <div class="col boot-lg visible-lg">lg</div>
        </div>
    </div>
<?php endif;?>

<div class="wrapper-darkblack">
	<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/header.php'); // Стандартная шапка ?>
</div>


<header class="header">
	<div class="container">
			<div class="row">
					<div class="col-xs-12 header__title">
							<h1>Коктейли для эффективных тренировок</h1>
					</div>
					<div class="col-sm-6 col-xs-12 header__left">
                        <div class="tr">
                            <div class="td header__box">
                                <img src="/img/box_left.jpg" alt="" class="img-responsive">
                                <img src="/img/box_arrow_left.png" alt="" class="header__arrow hidden-sm hidden-xs">
                                <a href="#main_order" class="tsr-btn btn btn-red-filled header__button hidden-sm hidden-xs">Выбрать вкус</a>
                            </div>
                            <div class="td header__left">
                                <h2>TO BE MUSCLE</h2>
                                <p>Протеиновые коктейли для набора мышечной массы</p>
                                <a href="#main_order" class="tsr-btn btn btn-red-filled hidden-lg hidden-md">Выбрать вкус</a>
                            </div>
                        </div>
					</div>
					<div class="col-sm-6 col-xs-12 header__right">
                        <div class="tr">
                            <div class="td header__right">
                                <h2>TO BE SLIM</h2>
                                <p>Коктейли с L-карнитином для тренировок и сжигания подкожного жира</p>
                                <a href="#main_order" class="tsr-btn btn btn-yellow-filled hidden-lg hidden-md">Выбрать вкус</a>
                            </div>
                            <div class="td header__box">
                                <img src="/img/box_right.jpg" alt="" class="img-responsive">
                                <img src="/img/box_arrow_right.png" alt="" class="header__arrow hidden-sm hidden-xs">
                                <a href="#main_order" class="tsr-btn btn btn-yellow-filled header__button hidden-sm hidden-xs">Выбрать вкус</a>
                            </div>
                        </div>
					</div>
			</div>
	</div>
</header>

<div class="effects-wrapper wrapper-grey hidden-sm hidden-xs" id="main_effects">
	<div class="container">
		<div class="std-header">С коктейлями To be вы</div>
		<div class="effects-inner">
			<div class="effects-block">
				<img src="/img/land_main/effect-1.png" alt="" />
				<span>Начнете питаться правильно</span>
				Вам больше не потребуется составлять подходящий рацион. To be - это спортивный питательный комплекс, в котором есть все необходимое для организма в процессе тренировок.
			</div>
			<div class="effects-block">
				<img src="/img/land_main/effect-2.png" alt="" />
				<span>Повысите эффективность тренировок</span>
				Комплекс из L-карнитина, быстрых и медленных белков обеспечит ваши мышцы питанием. Благодаря этому вы сможете наращивать мышечную массу быстрее.
			</div>
			<div class="effects-block">
				<img src="/img/land_main/effect-3.png" alt="" />
				<span>Не будете тратить время</span>
				На смешивание коктейлей в шейкере и приготовление рациона. Коктейль To be удобно разделён на порции, его можно взять с собой и выпить сразу после тренировки, предотвращая катаболизм
			</div>
			<div class="effects-block">
				<img src="/img/land_main/effect-4.png" alt="" />
				<span>Станете выглядеть лучше</span>
				Главная составляющая успешных занятий в зале - работа над собой и правильный рацион. Сосредоточьтесь на тренировках, а To be позаботится об остальном.
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="container components">
    <div class="row">
        <div class="col-xs-12">
            <h2>Что входит в тренировочный <span class="nobr">комплекс To be?</span></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <h3>Протеиновые коктейли <span class="nobr">To be Muscle</span></h3>
            <p>To be Muscle содержит смесь из быстрых и медленных белков, которые позволяют интенсивно тренироваться и эффективно наращивать мышечную массу, избегая катаболизма после тренировок</p>
        </div>
        <div class="col-sm-6 hidden-xs">
            <h3>Коктейли с L-карнитином <span class="nobr">To be Slim</span></h3>
            <p>Этот коктейль содержит L-карнитин - аминокислоту, которая насыщает организм энергией перед тренировкой, повышает выносливость и стимулирует сжигание подкожного жира.</p>
        </div>
    </div>
    <div class="row" style="margin: 50px 0;">
        <div class="col-xs-12">

            <div class="row">
                <div class="col-sm-6 col-xs-12" style="text-align: center;">

                    <div class="components__wrap">

                        <img src="/img/box_content1.jpg" alt="">
                        <div>
                            <img src="/img/plus_yellow.png" alt="+" class="components__yellow components__plus components__plus-y1">
                            <div class="components__boxes-desc components__boxes-desc-y1">
                                <h3>Изолят молочного белка</h3>
                                <p>Изолят состоит преимущественно из "быстрых" белков, которые дают необходимую энергию для интенсивных тренировок.</p>
                            </div>
                        </div>
                        <div>
                            <img src="/img/plus_yellow.png" alt="+" class="components__yellow components__plus components__plus-y2">
                            <div class="components__boxes-desc components__boxes-desc-y2">
                                <h3>Мицеллярный казеин</h3>
                                <p>Этот белок отличается от изолята медленным усвоением, что позволит восстановить организм после тренировки и избежать катаболизма, разрушающего мышечные волокна.</p>
                            </div>
                        </div>
                        <div>
                            <img src="/img/plus_yellow.png" alt="+" class="components__yellow components__plus components__plus-y3">
                            <div class="components__boxes-desc components__boxes-desc-y3">
                                <h3>Обезжиренное молоко</h3>
                                <p>Основа To be Muscle, в котором смешиваются остальные ингредиенты. Мы используем только качественное молоко одного из крупнейших производителей в России.</p>
                            </div>
                        </div>
                        <div>
                            <img src="/img/plus_yellow.png" alt="+" class="components__yellow components__plus components__plus-y4">
                            <div class="components__boxes-desc components__boxes-desc-y4">
                                <h3>Сукралоза</h3>
                                <p>Подсластитель, делающий коктейли по-настоящему вкусными. Это альтернатива сахару, не содержащая калорий и являющаяся безопасной для организма.</p>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="visible-xs col-xs-12" style="margin-bottom: 40px;">
                    <h3>Коктейли с L-карнитином <span class="nobr">To be Slim</span></h3>
                    <p>Этот коктейль содержит L-карнитин - аминокислоту, которая насыщает организм энергией перед тренировкой, повышает выносливость и стимулирует сжигание подкожного жира.</p>
                </div>



                <div class="col-sm-6 col-xs-12" style="text-align: center;">

                    <div class="components__wrap">
                        <img src="/img/box_content2.jpg" alt="" class="img-responsive">
                        <div>
                            <img src="/img/plus_red.png" alt="+" class="components__red components__plus components__plus-r1">
                            <div class="components__boxes-desc components__boxes-desc-r1">
                                <h3>L-карнитин</h3>
                                <p>Природное вещество, родственное витаминам группы В. L-карнитин повышает выносливость, стимулирует уменьшение подкожного жира, стабилизирует иммунную систему и нормализует уровень холестерина в крови. Этот компонент особенно эффективен в сочетании с занятиями фитнесом, спортивными тренировками и активным образом жизни.</p>
                            </div>
                        </div>
                        <div>
                            <img src="/img/plus_red.png" alt="+" class="components__red components__plus components__plus-r2">
                            <div class="components__boxes-desc components__boxes-desc-r2">
                                <h3>Молочная сыворотка</h3>
                                <p>Основа To be Slim, которая содержит 0 калорий. В состав сыворотки входит большое количество полезных микроэлементов и витаминов.</p>
                            </div>
                        </div>
                        <div>
                            <img src="/img/plus_red.png" alt="+" class="components__red components__plus components__plus-r3">
                            <div class="components__boxes-desc components__boxes-desc-r3">
                                <h3>Натуральный сок</h3>
                                <p>В отличие от большинства аналогов, имеющих в составе красители и ароматизаторы, коктейли To be Slim содержат натуральный сок.</p>
                            </div>
                        </div>
                        <div>
                            <img src="/img/plus_red.png" alt="+" class="components__red components__plus components__plus-r4">
                            <div class="components__boxes-desc components__boxes-desc-r4">
                                <h3>Сукралоза</h3>
                                <p>Подсластитель, делающий коктейли по-настоящему вкусными. Это альтернатива сахару, не содержащая калорий и являющаяся безопасной для организма.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="reviews-wrapper wrapper-grey hidden-sm hidden-xs" id="main_reviews">
	<div class="container">
		<div class="std-header">Отзывы о To be специалистов и спортсменов</div>
		<div class="reviews-inner">
			<div class="review">
				<div class="review-person">
					<img src="/img/land_main/reviews/p1.jpg" alt="" />
					<span>Анна Чуракова</span>
					Профессиональная спортсменка, чемпион Европы по версии WBFF
				</div>
				<div class="review-text">
					<span>Это невероятно вкусно и практично</span>
					<p>Рекомендуемая разовая порция протеина, которую удобно взять с собой на тренировку. To be — это вкусный и полезный напиток, уже готовый к употреблению.</p>
					<a data-fancybox="" href="https://www.youtube.com/watch?v=ZFJgdLC-PkE&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0" class="review-btn btn btn-red-bordered">Видеоотзыв</a>
				</div>
				<div class="clearer"></div>
			</div>
		</div>
	</div>
</div>

<div class="prop-wrapper hidden-sm hidden-xs" id="main_properties">
	<div class="container">
		<div class="std-header">5 важных свойств коктейлей To be</div>
		<div class="prop-inner">
			<div class="prop-block">
				<img src="/img/land_main/prop-1.png" alt="" />
				<span>Приятный <br/>вкус</span>
				To be создан одним из крупнейших в России производителей молока. <br/>
				Наша задача - изготовление по-настоящему вкусных продуктов, и To be - не исключение.
			</div>
			<div class="prop-block">
				<img src="/img/land_main/prop-2.png" alt="" />
				<span>Длительный срок хранения</span>
				Благодаря технологии производства и асептической упаковке, срок годности: Muscle - 6 месяцев, Slim - 4 месяца.
			</div>
			<div class="prop-block">
				<img src="/img/land_main/prop-3.png" alt="" />
				<span>Высокое <br/>качество</span>
				Продукт прошел сертификацию и имеет все необходимые лицензии
			</div>
			<div class="clearer"></div>
			<div class="prop-block">
				<img src="/img/land_main/prop-4.png" alt="" />
				<span>Продуманный состав</span>
				Коктейли To be представляют собой комплекс, состоящий из протеиновых коктейлей и коктейлей с L-карнитином.<br/>
				Это позволяет поддерживать организм, насыщая его энергией до тренировки, и восстанавливая после.
			</div>
			<div class="prop-block">
				<img src="/img/land_main/prop-5.png" alt="" />
				<span>Низкое <br/>содержание жира</span>
				To be slim не содержит жира, а To be muscle содержит всего 0,1-0,2% жира. При этом коктейли утоляют голод на 4-6 часов.
			</div>
			<div class="clearer"></div>
		</div>
	</div>
</div>

<div class="order-wrapper wrapper-black" id="main_order">
	<div class="container">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/templates/order.php'); // Стандартный блок заказа ?>
	</div>
</div>


<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'); // Подвал сайта ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/modals.php'); // Стандартные всплывайки ?>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/foot_site.php'); // Стандартные скрипты ?>

</body>
</html>